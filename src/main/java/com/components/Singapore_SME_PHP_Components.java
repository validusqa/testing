package com.components;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
/*import org.sikuli.script.Key;
import org.sikuli.script.Screen;*/
import org.sikuli.script.Screen;

import com.baseClasses.BaseClass_Web;
import com.baseClasses.PDFResultReport;
import com.baseClasses.ThreadLocalWebdriver;
import com.objectRepository.Singapore_VUE_Locators;
import com.objectRepository.ValidusRegisterAsInvester;
import com.objectRepository.ValidusSmoke_Loc;

import bsh.org.objectweb.asm.Type;

public class Singapore_SME_PHP_Components extends BaseClass_Web {

	public Singapore_VUE_Locators singapore_VUE_Locators=new Singapore_VUE_Locators();
	public ValidusRegisterAsInvester registerasInvestorlocators = new ValidusRegisterAsInvester();
	public ValidusSmoke_Loc validussmokelocators = new ValidusSmoke_Loc();
	public static String FundsAvailableForLending;
	public static String CommitmentOfLending;
	public static String AmountRequestedForWithdrawal;
	public static String LoanPortfolio;
	public static String NetReturn;
	public static String TheAmountOfFundsToDateIs;
	public static String TheAmountOfFundsWithdrawnToDateIs;
	public static String emailIDInvestor;
	public static String emailIDInvestor1;
	public Screen s = new Screen();
	Singapore_VUE_Components singapore_VUE_Components=new Singapore_VUE_Components(pdfResultReport);
	public Singapore_SME_PHP_Components(PDFResultReport pdfresultReport) {
		this.pdfResultReport = pdfresultReport;
	}

	public void openURL() throws Exception {
		try {
			launchapp(pdfResultReport.testDataValue.get("AppURL"));
			waitForObj(3000);

			pdfResultReport.addStepDetails("openURL", "Application should open the url",
					"Successfully opened the URL" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("openURL", "Application should open the url",
					"Unable to open the URL" + e.getMessage(), "Fail", "N");
		}

	}

	public void mainPagelogin() throws Throwable {
		try {

			click(registerasInvestorlocators.pinjamnHomepage);
			waitForObj(3000);
			click(registerasInvestorlocators.resetPassword);
			pdfResultReport.addStepDetails("Main Page login", "Application should click on the login button",
					"Successfully clicked into the login" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("Main Page login", "Unable to click on the login button",
					"Unable to click on login into the application" + e.getMessage(), "Fail", "N");
		}
	}

	public void resetPasswordlogin() throws Throwable {

		try {
			launchapp(pdfResultReport.testDataValue.get("AppURL"));
			System.out.println("emailIDInvestor :::" + ValidusSmoke_Components.emailIDInvestor);
			js_type(validussmokelocators.emailIDInvestor, ValidusSmoke_Components.emailIDInvestor, "username");
			set(validussmokelocators.passwordInvestor, pdfResultReport.testData.get("PasswordInvestor"));
			click(registerasInvestorlocators.resetPasswordPinajaman);
			// click(registerasInvestorlocators.pinjamnHomepage);
			// click(registerasInvestorlocators.resetPassword);
			waitForObj(3000);
			pdfResultReport.addStepDetails("Login to ResetPassword",
					"Application should successfully login to the application for reset password",
					"successfully logged in to the application for  reset password" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("Login to ResetPassword",
					"Not able to login to the application for reset a passwordn",
					"Unable to login to the application for reset a passwordn" + e.getMessage(), "Fail", "N");
		}
	}

	public void resetPassword() throws Throwable {

		try {
			set(registerasInvestorlocators.resetEmailaddress, pdfResultReport.testData.get("UserName"));
			waitForObj(3000);
			click(registerasInvestorlocators.resetPasswordsubmitButton);
			waitForObj(10000);
			pdfResultReport.addStepDetails("ResetPassword Page",
					"User should successfully click on submit button of the resetpassword",
					"Successfully clicked on the submit button for resetpassword" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("ResetPassword Page",
					"User is not able to click on submit button of the resetpassword",
					"Unable to click on submit button of the resetpassword" + e.getMessage(), "Fail", "N");
		}
	}

	public void login() throws Throwable {
		try {
			js_type(validussmokelocators.emailIDInvestor, Singapore_SME_PHP_Components.emailIDInvestor, "username");
			set(validussmokelocators.passwordInvestor, pdfResultReport.testData.get("PasswordInvestor"));
			click(registerasInvestorlocators.login);
			waitForObj(7000);
			pdfResultReport.addStepDetails("login", "Application should login",
					"Successfully loggedIn into the application" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("login", "Unable to open the url",
					"Unable to login into the application" + e.getMessage(), "Fail", "N");
		}
	}

	public void SMElogin() throws Throwable {
		try {
			// click(validussmokelocators.validusMainpageLogin);
			js_type(validussmokelocators.emailIDInvestor, Singapore_SME_PHP_Components.emailIDInvestor, "username");
			
			set(validussmokelocators.passwordInvestor, pdfResultReport.testData.get("PasswordInvestor"));
			click(registerasInvestorlocators.login);
			waitForObj(5000);
			pdfResultReport.addStepDetails("SMElogin", "Application should login",
					"Successfully loggedIn into the application" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("SMElogin", "Application should login",
					"Unable to login into the application" + e.getMessage(), "Fail", "N");
		}
	}

	public void banklogin() throws Throwable {
		try {
			click(registerasInvestorlocators.pinjamnHomepage);

			js_type(validussmokelocators.emailIDInvestor, "nag0213124114@gmailx.com", "username");
			// js_type(validussmokelocators.emailIDInvestor,
			// ValidusSmoke_Components.emailIDInvestor, "username");
			set(validussmokelocators.passwordInvestor, pdfResultReport.testData.get("PasswordInvestor"));
			click(registerasInvestorlocators.login);
			waitForObj(10000);
			pdfResultReport.addStepDetails("banklogin", "Application should login",
					"Successfully loggedIn into the application" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("banklogin", "Unable to open the url",
					"Unable to login into the application" + e.getMessage(), "Fail", "N");
		}
	}

	public void loginPinjamanInvestor() throws Throwable {
		try {
			// click(validussmokelocators.validusMainpageLogin);
			waitForObj(1000);
			// js_type(validussmokelocators.emailIDInvestor, "nag0423104409@gmailx.com",
			// "username");
			// waitForObj(1000);
			js_type(validussmokelocators.emailIDInvestor, ValidusSmoke_Components.emailIDInvestor, "username");
			set(validussmokelocators.passwordInvestor, pdfResultReport.testData.get("PasswordInvestor"));
			click(registerasInvestorlocators.login);
			waitForObj(5000);
			pdfResultReport.addStepDetails("loginPinjamanInvestor", "Application should login",
					"Successfully loggedIn into the application" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("loginPinjamanInvestor", "Application should login",
					"Unable to login into the application" + e.getMessage(), "Fail", "N");
		}
	}

	public void STWCFacility() throws Throwable {
		try {

			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//i[@class='fa fa-puzzle-piece'])[1]")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//label[@for='LoanProductType2']")).click();
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='LoanAmount']")).sendKeys("5000");
			new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='LoanTenure']")))
					.selectByVisibleText("6 months");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='NumOfEmployees']")).sendKeys("5");

			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='AnnualRevenuePreviousYear']"))
					.sendKeys("5000");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='AnnualRevenueCurrentYear']"))
					.sendKeys("5000");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='NetProfitPreviousYear']"))
					.sendKeys("-5000");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='NetProfitCurrentYear']"))
					.sendKeys("-5000");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='NamePG1']")).sendKeys("Tarun");
			new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='ResidentialStatusPG1']")))
					.selectByVisibleText("Singapore Citizen");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='TaxIncomePreviousYearPG1']"))
					.sendKeys("2017");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='TaxIncomeCurrentYearPG1']"))
					.sendKeys("2018");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='NamePG2']")).sendKeys("Saketh");
			new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='ResidentialStatusPG2']")))
					.selectByVisibleText("Singapore Citizen");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='TaxIncomePreviousYearPG2']"))
					.sendKeys("2017");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='TaxIncomeCurrentYearPG2']"))
					.sendKeys("2018");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='NamePG3']")).sendKeys("Naresh");
			new Select(ThreadLocalWebdriver.getDriver().findElement(By.xpath("//select[@id='ResidentialStatusPG3']")))
					.selectByVisibleText("Singapore Citizen");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='TaxIncomePreviousYearPG3']"))
					.sendKeys("2017");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("//input[@id='TaxIncomeCurrentYearPG3']"))
					.sendKeys("2018");
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//button[@id='submit'])[1]")).click();
			Thread.sleep(6000);
			uploadFile(registerasInvestorlocators.STWCMand);
			Thread.sleep(8000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//button[@id='submit'])[2]")).click();

			pdfResultReport.addStepDetails("STWCFacility", "Apply facility for STWC",
					"Apply facility should create successfully" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("STWCFacility", "Unable Apply facility for STWC",
					"Unable to apply facility to STWC" + e.getMessage(), "Fail", "N");
		}
	}

	public void navigateTocompanyIncorporateInSingapore() throws Throwable {
		try {
			waitForObj(4000);
			JSClick(registerasInvestorlocators.companyIncorporateInSingapore, "companyIncorporateInSingapore");
			waitForObj(3000);
			pdfResultReport.addStepDetails("navigateTocompanyIncorporateInSingapore",
					"User should click on companyIncorporateInSingapore",
					"User successfully clicked on companyIncorporateInSingapore" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("navigateTocompanyIncorporateInSingapore",
					"User is not able to click on companyIncorporateInSingapore",
					"Unable to open the click on companyIncorporateInSingapore" + e.getMessage(), "Fail", "N");
		}
	}

	public void navigateToIndonesianLegalEntity() throws Throwable {
		try {
			waitForObj(4000);
			JSClick(registerasInvestorlocators.indonesianLegalEntity, "companyincorporate");
			waitForObj(3000);
			pdfResultReport.addStepDetails("navigateToIndonesianLegalEntity",
					"User should click on IndonesianLegalEntity",
					"User successfully clicked on IndonesianLegalEntity" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("navigateToIndonesianLegalEntity",
					"User is not able to click on IndonesianLegalEntity",
					"Unable to open the click on IndonesianLegalEntity" + e.getMessage(), "Fail", "N");
		}
	}

	public void navigateTocompanyIncorporateOutsideSingapore() throws Exception {
		try {
			waitForElement(registerasInvestorlocators.companyIncorporateOutsideSingapore, 60);
			click(registerasInvestorlocators.companyIncorporateOutsideSingapore);
			waitForObj(3000);
			pdfResultReport.addStepDetails("navigateTocompanyIncorporateOutsideSingapore",
					"User should click on companyIncorporateOutsideSingapore",
					"User successfully clicked on companyIncorporateOutsideSingapore" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("navigateTocompanyIncorporateInSingapore",
					"User is not able to click on companyIncorporateOutsideSingapore",
					"Unable to open the click on companyIncorporateOutsideSingapore" + e.getMessage(), "Fail", "N");
		}
	}

	public void navigateToindividualInvestorSingaporeCitizen() throws Exception {
		try {
			waitForElement(registerasInvestorlocators.individualInvestorSingaporeCitizen, 60);
			click(registerasInvestorlocators.individualInvestorSingaporeCitizen);
			waitForObj(3000);
			pdfResultReport.addStepDetails("navigatetoindividualInvestorSingaporeCitizen",
					"User should click on individualInvestorSingaporeCitizen",
					"User successfully clicked on individualInvestorSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("navigatetoindividualInvestorSingaporeCitizen",
					"User is not able to click on individualInvestorSingaporeCitizen",
					"Unable to open the click on individualInvestorSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}

	public void navigateToindividualInvestorNonSingaporeCitizen() throws Throwable {
		try {
			waitForElement(registerasInvestorlocators.individualInvestorNonSingaporeCitizen, 60);
			JSClick(registerasInvestorlocators.individualInvestorNonSingaporeCitizen,
					"individualInvestorNonSingaporeCitizen");
			waitForObj(4000);
			pdfResultReport.addStepDetails("navigatetoindividualInvestorNonSingaporeCitizen",
					"User should click on individualInvestorNonSingaporeCitizen",
					"User successfully clicked on individualInvestorNonSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to open the URL" + e.getMessage());
			pdfResultReport.addStepDetails("navigatetoindividualInvestorNonSingaporeCitizen",
					"User is not able to click on individualInvestorNonSingaporeCitizen",
					"User unable to click on individualInvestorNonSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}

	public void uploadFile(By loc) throws Throwable {
		try {
			Screen s = new Screen();
			Thread.sleep(2000);
			// s.click("E:\\SeleniumMavenProject\\SeleniumMavenProject\\MediaFiles\\Selectfile1.PNG");
			click(loc);
			Thread.sleep(4000);
		//	s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/testtpdf.png");
		//	s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/openn.png");
			s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/File.PNG", System.getProperty("user.dir")+"\\MediaFiles\\passportfront.pdf");
			s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/OpenWin.PNG");
			Thread.sleep(4000);
		} catch (Exception e) {
			System.out.println("Unable to upload the file");
		}
	}
	
	public void uploadFile(By loc, String fileName) throws Throwable {
		try {
			//
		//	Thread.sleep(2000);
			click(loc);
			Thread.sleep(4000);
			Robot r=new Robot();
			r.keyPress(KeyEvent.VK_CONTROL);
			r.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(1000);
		//	s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/testtpdf.png");
		//	s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/openn.png");
			s.type(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/File.PNG", System.getProperty("user.dir")+"\\MediaFiles\\"+fileName+".pdf");
			s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/OpenWin.PNG");
			Thread.sleep(4000);
		} catch (Exception e) {
			System.out.println("Unable to upload the file");
		}
	}
	public void pageDown() {
		try {
			Actions a = new Actions(ThreadLocalWebdriver.getDriver());
			a.sendKeys(Keys.PAGE_DOWN).build().perform();
			Thread.sleep(2000);
		} catch (Exception e) {
			System.out.println("Unable to upload the file");
		}
	}

	public void CreateAccountSME() throws Throwable {
		try {
			// click(By.xpath("//ul[@id='menu-topbar-menu']//li/a[text()='Masuk']"));
			// click(validussmokelocators.validusMainpageLogin);
		/*	click(validussmokelocators.validusCreateanAccount);
			waitForElement(validussmokelocators.validusSME, 30);
			waitForObj(3000);
			click(validussmokelocators.validusSME);
			waitForElement(validussmokelocators.Setuju1, 30);
			waitForObj(2000);
			click(validussmokelocators.Setuju1);*/
		//	waitForObj(2000);
			click(By.xpath("//*[@id='app']/div/div[2]/div[2]/div/div[1]/div/ul/li[1]/span"));
			SimpleDateFormat df = new SimpleDateFormat("MMddhhmmss");
			Date d = new Date();
			String time = df.format(d);
			System.out.println("time::" + time);
			emailIDInvestor1 = "nag" + time;
			System.out.println("emailID SME1 ::" + emailIDInvestor1);
			emailIDInvestor = emailIDInvestor1 + "@gmailx.com";
			System.out.println("emailID SME :::" + emailIDInvestor);
			//js_type(validussmokelocators.emailIDInvestor, emailIDInvestor, "EmailId");
            Thread.sleep(1000);
			set(validussmokelocators.emailIDInvestor, emailIDInvestor);
			File f = new File(System.getProperty("user.dir").replace("\\", "/") + "/MailIds.txt");
			FileWriter fw = new FileWriter(f, true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(emailIDInvestor);
			bw.newLine();

			bw.close();
			fw.close();
			Thread.sleep(5000);
			SimpleDateFormat df1 = new SimpleDateFormat("hhmmss");
			Date d1 = new Date();
			String time1 = df1.format(d1);
			String time2 = "6" + time1;
			System.out.println("time2::" + time2);
			click(validussmokelocators.mobileNumber);
			Thread.sleep(2000);
     		set(validussmokelocators.mobileNumber, time2);
			Thread.sleep(2000);
		    js_type(validussmokelocators.mobileNumber, time2, "mobilenumber");
			Thread.sleep(1000);
			set(validussmokelocators.mobileNumber, "99");
     		click(validussmokelocators.mobileNumber);
			tab();
			Thread.sleep(2000);        
            set(validussmokelocators.passwordInvestor, "Admin@1234");
			
			Thread.sleep(2000);
			tab();
			set(validussmokelocators.confirmPasswordInvestor, "Admin@1234");
			Thread.sleep(2000);
			tab();
			 
            
			click(validussmokelocators.SmeloginTermsandCondition);
			Thread.sleep(2000);
			click(validussmokelocators.investorContinue);
		//	ThreadLocalWebdriver.getDriver().navigate().forward();
			
			waitForObj(3000);
			pdfResultReport.addStepDetails("Create account", "Application should allow the user to create account",
					"Successfully created an account" + " ", "Pass", "Y");

		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("Create account", "Application should allow the user to create account",
					"Unable to the create account" + e.getMessage(), "Fail", "N");

		}
	}

	public void SingaporeCreateAccountSME() throws Exception {
		try {
			// click(validussmokelocators.validusMainpageLogin);
			click(validussmokelocators.validusCreateanAccount);
			waitForElement(validussmokelocators.singaporeSME, 30);
			waitForObj(3000);
			click(validussmokelocators.singaporeSME);
			/*
			 * SimpleDateFormat df = new SimpleDateFormat("yyyyMMddhhmmss"); Date d = new
			 * Date(); String time = df.format(d); System.out.println("time::" + time);
			 * emailIDInvestor = time + "@gmail.com";
			 */
			// set(validussmokelocators.emailIDInvestor, emailIDInvestor);
			set(validussmokelocators.emailIDInvestor, pdfResultReport.testData.get("EmailIDInvestor"));
			set(validussmokelocators.passwordInvestor, pdfResultReport.testData.get("PasswordInvestor"));
			set(validussmokelocators.confirmPasswordInvestor, pdfResultReport.testData.get("ConfirmPasswordInvestor"));

			SimpleDateFormat df1 = new SimpleDateFormat("hhmmss");
			Date d1 = new Date();
			String time1 = df1.format(d1);
			System.out.println("time1::" + time1);

			set(validussmokelocators.mobileNumber, time1);
			click(validussmokelocators.SmeloginTermsandCondition);
			click(validussmokelocators.investorContinue);

			waitForObj(3000);
			pdfResultReport.addStepDetails("Create account", "Application should allow the user to enter details",
					"Successfully created an account" + " ", "Pass", "Y");
		} catch (Exception e) {
			System.out.println(e);
			log.fatal("Unable to create account" + e.getMessage() + "Line Number :" + e.getStackTrace());
			pdfResultReport.addStepDetails("Create account", "Unable to enter details",
					"Unable to create account" + e.getMessage(), "Fail", "N");

		}
	}

	public void companyIncorporatedInSingapore() throws Throwable {
		try {
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
			select(registerasInvestorlocators.title, pdfResultReport.testData.get("Title"));
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.companyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.deedofEstb, pdfResultReport.testData.get("DeedOfEstd"));
			set(registerasInvestorlocators.middleName, pdfResultReport.testData.get("Middle Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			set(registerasInvestorlocators.postalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.SICCCode, pdfResultReport.testData.get("SICC (Industry) Code"));
			select(registerasInvestorlocators.Nationality, pdfResultReport.testData.get("Nationality"));
			set(registerasInvestorlocators.registrationDate, pdfResultReport.testData.get("Date of Registration"));
			set(registerasInvestorlocators.primaryContactNo, pdfResultReport.testData.get("PrimaryCntctNo"));
			set(registerasInvestorlocators.designationInv, pdfResultReport.testData.get("Designation"));
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.NRICPassportFront1);
			waitForObj(8000);
			// pageDown();
			uploadFile(registerasInvestorlocators.NRICPassportBack);
			waitForObj(7000);
			uploadFile(registerasInvestorlocators.proofofResidence);
			waitForObj(7000);
			// Robot r=new Robot();
			// r.keyPress(KeyEvent.VK_PAGE_DOWN);
			uploadFile(registerasInvestorlocators.memoandArticles);
			waitForObj(7000);
			uploadFile(registerasInvestorlocators.acraBizfile);
			waitForObj(7000);
			// uploadFile(registerasInvestorlocators.optionalFile);
			waitForObj(5000);
			click(registerasInvestorlocators.agree);
			click(registerasInvestorlocators.acknowledge);
			click(registerasInvestorlocators.termsandCondition);
			click(registerasInvestorlocators.registerUs);
			JSClick(registerasInvestorlocators.submit, "Submit Button");
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//h3[@class='profile-ban-txt']"))
					.getText();
			System.out.println(str);
			switchwindow(0);
			waitForObj(3000);
			pdfResultReport.addStepDetails("Register as companyIncorporatedInSingapore",
					"User should register as companyIncorporatedInSingapore",
					"Successfully registered as companyIncorporatedInSingapore" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as companyIncorporatedInSingapore",
					"User is not able to register as companyIncorporatedInSingapore",
					"User unable to register as companyIncorporatedInSingapore" + e.getMessage(), "Fail", "N");
		}
	}

	public void PinjamancompanyIncorporatedInSingaporeOld() throws Throwable {
		try {
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
			select(registerasInvestorlocators.title, pdfResultReport.testData.get("Title"));
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			// set(registerasInvestorlocators.deedofEstb,
			// pdfResultReport.testData.get("DeedOfEstd"));
			click(registerasInvestorlocators.deedofEstb);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("/html/body/div[5]/div[1]/table/tbody/tr[3]/td[6]"))
					.click();
			set(registerasInvestorlocators.companyAddress, pdfResultReport.testData.get("CompanyAddress"));

			set(registerasInvestorlocators.familyName, pdfResultReport.testData.get("FamilyName"));
			// set(registerasInvestorlocators.nricName,
			// pdfResultReport.testData.get("NricName"));
			// set(registerasInvestorlocators.companyZipCode,
			// pdfResultReport.testData.get("CompanyZipCode"));
			set(registerasInvestorlocators.netWorthPreviousYear, "100000");
			set(registerasInvestorlocators.netWorthCurrentYear, "200000");
			select(registerasInvestorlocators.nationality, pdfResultReport.testData.get("Nationality"));
			selectbyvalue(registerasInvestorlocators.siCC, "Barang Kebutuhan Hidup Lainnya");
			set(registerasInvestorlocators.primaryContactNumberPinjaman,
					pdfResultReport.testData.get("PrimaryCntctNo"));
			set(registerasInvestorlocators.designationInv, pdfResultReport.testData.get("Designation"));
			set(registerasInvestorlocators.managementName, "bahasa");
			set(registerasInvestorlocators.directorPosition, "CEO");
			click(registerasInvestorlocators.directorServingSince);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("/html/body/div[5]/div[1]/table/tbody/tr[3]/td[6]"))
					.click();
			set(registerasInvestorlocators.directorPassportNumber, "AP1228278728");
			set(registerasInvestorlocators.directorCitizenship, "Test");
			set(registerasInvestorlocators.directorContactNo, pdfResultReport.testData.get("PrimaryCntctNo"));
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//input[@name='shareholder_ownership[]'])[1]"))
					.clear();
			set(registerasInvestorlocators.shareholderOwnership, "30");
			set(registerasInvestorlocators.shareholderName, "NithinB");
			selectbyvalue(registerasInvestorlocators.noOFShareholders, "3");
			set(registerasInvestorlocators.middleName, pdfResultReport.testData.get("Middle Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			set(registerasInvestorlocators.postalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			// set(registerasInvestorlocators.SICCCode, pdfResultReport.testData.get("SICC
			// (Industry) Code"));
			select(registerasInvestorlocators.Nationality, pdfResultReport.testData.get("Nationality"));
			// set(registerasInvestorlocators.registrationDate,
			// pdfResultReport.testData.get("Date of Registration"));
			// set(registerasInvestorlocators.shareholderOwnership, "Test");
			set(registerasInvestorlocators.shareholderPassportnumber, "AP1228278728");
			set(registerasInvestorlocators.shareholderCitizenship, "ShareholderCitizenship");
			set(registerasInvestorlocators.shareholderContactnumber, pdfResultReport.testData.get("PrimaryCntctNo"));
			set(registerasInvestorlocators.shareholderCitizen1, "shareholderCitizen1");
			set(registerasInvestorlocators.shareholderContactnumber1, pdfResultReport.testData.get("PrimaryCntctNo"));

			// set(registerasInvestorlocators.primaryContactNo,
			// pdfResultReport.testData.get("PrimaryCntctNo"));
			set(registerasInvestorlocators.shareholderOwnership3, "50");
			set(registerasInvestorlocators.shareholderPassportNumber, "AP1338278728");
			set(registerasInvestorlocators.shareholderOwnership3, "20");
			set(registerasInvestorlocators.shareholderPassport3, "AP1228278728");
			set(registerasInvestorlocators.shareholderCitizenship3, "shareholderCitizenship3");
			set(registerasInvestorlocators.shareholderContactnumber3, pdfResultReport.testData.get("PrimaryCntctNo"));

			waitForObj(2000);
			uploadFile(registerasInvestorlocators.NRICPassportFront1);
			waitForObj(8000);
			// pageDown();
			uploadFile(registerasInvestorlocators.NRICPassportBack);
			waitForObj(7000);
			uploadFile(registerasInvestorlocators.proofofResidence);
			waitForObj(7000);
			// Robot r=new Robot();
			// r.keyPress(KeyEvent.VK_PAGE_DOWN);
			uploadFile(registerasInvestorlocators.memoandArticles);
			waitForObj(7000);
			uploadFile(registerasInvestorlocators.acraBizfile);
			waitForObj(7000);
			// uploadFile(registerasInvestorlocators.optionalFile);
			// waitForObj(5000);
			click(registerasInvestorlocators.acknowledge1);
			click(registerasInvestorlocators.acknowledge);
			click(registerasInvestorlocators.termsandCondition);
			click(registerasInvestorlocators.registerUs);
			click(registerasInvestorlocators.agree);
			waitForObj(2000);
			pageDown();
			click(registerasInvestorlocators.submit);
			// JSClick(registerasInvestorlocators.submit, "Submit Button");
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//h3[@class='profile-ban-txt']"))
					.getText();
			System.out.println(str);
			switchwindow(0);
			waitForObj(3000);
			pdfResultReport.addStepDetails("Register as companyIncorporatedInSingapore",
					"User should register as companyIncorporatedInSingapore",
					"Successfully registered as companyIncorporatedInSingapore" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as companyIncorporatedInSingapore",
					"User is not able to register as companyIncorporatedInSingapore",
					"User unable to register as companyIncorporatedInSingapore" + e.getMessage(), "Fail", "N");
		}
	}

	public void PinjamancompanyIncorporatedInSingapore() throws Throwable {
		try {
			set(registerasInvestorlocators.companyName, ValidusSmoke_Components.emailIDInvestor1);
			// select(registerasInvestorlocators.title,
			// pdfResultReport.testData.get("Title"));
			select(registerasInvestorlocators.title, "Mr");
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			click(registerasInvestorlocators.deedofEstb);
			Thread.sleep(1000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(
					"(//div[@class='datepicker-days']//table[@class=' table-condensed']//tr//td[@class='day'])[1]"))
					.click();
			set(registerasInvestorlocators.companyAddress, pdfResultReport.testData.get("CompanyAddress"));
			set(registerasInvestorlocators.companyZipCode, "166166");
			set(registerasInvestorlocators.familyName, pdfResultReport.testData.get("FamilyName"));
			set(registerasInvestorlocators.netWorthPreviousYear, "100000");
			set(registerasInvestorlocators.netWorthCurrentYear, "200000");
			select(registerasInvestorlocators.nationality, pdfResultReport.testData.get("Nationality"));
			selectbyvalue(registerasInvestorlocators.siCC, "Barang Kebutuhan Hidup Lainnya");
			set(registerasInvestorlocators.primaryContactNumberPinjaman,
					pdfResultReport.testData.get("PrimaryCntctNo"));
			set(registerasInvestorlocators.designationInv, pdfResultReport.testData.get("Designation"));
			set(registerasInvestorlocators.managementName, "bahasa");
			set(registerasInvestorlocators.directorPosition, "CEO");
			click(registerasInvestorlocators.directorServingSince);
			Thread.sleep(1000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(
					"(//div[@class='datepicker-days']//table[@class=' table-condensed']//tr//td[@class='day'])[1]"))
					.click();
			set(registerasInvestorlocators.directorPassportNumber, "AP1228278728");
			select(registerasInvestorlocators.directorCitizenship, "Indonesia");
			set(registerasInvestorlocators.directorContactNo, pdfResultReport.testData.get("PrimaryCntctNo"));
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//input[@name='shareholder_ownership[]'])[1]"))
					.clear();

			selectbyvalue(registerasInvestorlocators.noOFShareholders, "3");
			set(registerasInvestorlocators.middleName, pdfResultReport.testData.get("Middle Name"));
			js_type(registerasInvestorlocators.NRICPassportNumber, "123456", "Nric");
			select(registerasInvestorlocators.Nationality, pdfResultReport.testData.get("Nationality"));
			/*
			 * set(registerasInvestorlocators.shareholderName, "Nagesh");
			 * set(registerasInvestorlocators.shareholderPassportnumber, "AP1228278728"); //
			 * set(registerasInvestorlocators.shareholderCitizenship,
			 * "ShareholderCitizenship")
			 * set(registerasInvestorlocators.shareholderCitizenship_NEW,
			 * "ShareholderCitizenship");
			 * select(registerasInvestorlocators.shareholder_citizenship, "Indonesia");
			 * set(registerasInvestorlocators.shareholderContactnumber,
			 * pdfResultReport.testData.get("PrimaryCntctNo"));
			 * set(registerasInvestorlocators.shareholderOwnership, "30");
			 * 
			 * set(registerasInvestorlocators.shareholderName1, "Nagesh2");
			 * set(registerasInvestorlocators.shareholderOwnership2,"2");
			 * set(registerasInvestorlocators.shareholderContactnumber1,
			 * pdfResultReport.testData.get("PrimaryCntctNo"));
			 * set(registerasInvestorlocators.shareholderPassportNumber, "AP1228278728");
			 * select(registerasInvestorlocators.shareholder_citizenship2, "Indonesia"); //
			 * set(registerasInvestorlocators.shareholderCitizenship_New,
			 * "ShareholderCitizenship");
			 * select(registerasInvestorlocators.shareholderCitizenship_New,
			 * "Pilih negara");
			 * 
			 * 
			 * set(registerasInvestorlocators.shareholderName2, "Nagesh3");
			 * js_type(registerasInvestorlocators.shareholderOwnership3,"5", "share");
			 * set(registerasInvestorlocators.shareholderPassport3, "AP1228278728"); //
			 * set(registerasInvestorlocators.shareholderCitizenship3,
			 * "shareholderCitizenship3");
			 * set(registerasInvestorlocators.shareholderContactnumber3,
			 * pdfResultReport.testData.get("PrimaryCntctNo"));
			 * select(registerasInvestorlocators.shareholder_citizenship3, "Indonesia");
			 * 
			 * 
			 * waitForObj(2000); uploadFile(registerasInvestorlocators.NRICPassportFront1);
			 * waitForObj(8000); // pageDown();
			 * uploadFile(registerasInvestorlocators.NRICPassportBack); waitForObj(7000);
			 * uploadFile(registerasInvestorlocators.proofofResidence); waitForObj(7000);
			 * //Robot r=new Robot(); // r.keyPress(KeyEvent.VK_PAGE_DOWN);
			 * uploadFile(registerasInvestorlocators.memoandArticles); waitForObj(7000);
			 * uploadFile(registerasInvestorlocators.acraBizfile); waitForObj(7000);
			 * 
			 * uploadFile(registerasInvestorlocators.actupload6); waitForObj(7000);
			 * uploadFile(registerasInvestorlocators.actupload7); waitForObj(7000);
			 * 
			 * //uploadFile(registerasInvestorlocators.optionalFile); //waitForObj(5000);
			 * click(registerasInvestorlocators.acknowledge1);
			 * click(registerasInvestorlocators.acknowledge);
			 * click(registerasInvestorlocators.termsandCondition); waitForObj(2000);
			 * click(registerasInvestorlocators.Secondcheckbox);
			 * click(registerasInvestorlocators.esignatureAgree); waitForObj(2000);
			 * click(registerasInvestorlocators.registerUs);
			 * click(registerasInvestorlocators.agree); //
			 * click(registerasInvestorlocators.agree5);
			 * 
			 * waitForObj(2000); pageDown(); click(registerasInvestorlocators.submit);
			 * waitForObj(2000); click(registerasInvestorlocators.confirmMSG);
			 * //JSClick(registerasInvestorlocators.submit, "Submit Button");
			 * 
			 * 
			 * String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(
			 * "//h3[@class='profile-ban-txt']")).getText(); System.out.println(str);
			 * switchwindow(0);
			 * 
			 * waitForObj(3000);
			 */
			pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore",
					"User should register as companyIncorporatedInSingapore in batumbu",
					"Successfully registered as companyIncorporatedInSingapore in batumbu" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore",
					"User should register as companyIncorporatedInSingapore in batumbu",
					"User unable to register as companyIncorporatedInSingapore in batumbu" + e.getMessage(), "Fail",
					"N");
		}
	}

	public void PinjamancompanyIncorporatedInSingapore_2() throws Throwable {
		try {
	set(registerasInvestorlocators.shareholderName, "Nagesh");
	set(registerasInvestorlocators.shareholderPassportnumber, "AP1228278728");
//	set(registerasInvestorlocators.shareholderCitizenship, "ShareholderCitizenship")
	set(registerasInvestorlocators.shareholderCitizenship_NEW, "ShareholderCitizenship");
	select(registerasInvestorlocators.shareholder_citizenship, "Indonesia");
	set(registerasInvestorlocators.shareholderContactnumber, pdfResultReport.testData.get("PrimaryCntctNo"));
	set(registerasInvestorlocators.shareholderOwnership, "30");
	pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore_2","User should register as companyIncorporatedInSingapore in batumbu","Successfully registered as companyIncorporatedInSingapore in batumbu" + " ","Pass", "Y");
}catch(

	Exception e)
	{
		log.fatal("Unable to Homelogin" + e.getMessage());
		pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore_2",
				"User should register as companyIncorporatedInSingapore in batumbu",
				"User unable to register as companyIncorporatedInSingapore in batumbu" + e.getMessage(), "Fail", "N");
	}
	}

	public void PinjamancompanyIncorporatedInSingapore_3() throws Throwable {
		try {

	
	set(registerasInvestorlocators.shareholderName1, "Nagesh2");
	set(registerasInvestorlocators.shareholderOwnership2,"2");
	set(registerasInvestorlocators.shareholderContactnumber1, pdfResultReport.testData.get("PrimaryCntctNo"));
	set(registerasInvestorlocators.shareholderPassportNumber, "AP1228278728");
	select(registerasInvestorlocators.shareholder_citizenship2, "Indonesia");
//	set(registerasInvestorlocators.shareholderCitizenship_New, "ShareholderCitizenship");
	select(registerasInvestorlocators.shareholderCitizenship_New, "Pilih negara");

	pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore_3","User should register as companyIncorporatedInSingapore in batumbu","Successfully registered as companyIncorporatedInSingapore in batumbu" + " ","Pass", "Y");
}catch(

	Exception e)
	{
		log.fatal("Unable to Homelogin" + e.getMessage());
		pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore_3",
				"User should register as companyIncorporatedInSingapore in batumbu",
				"User unable to register as companyIncorporatedInSingapore in batumbu" + e.getMessage(), "Fail", "N");
	}
	}
	
	public void PinjamancompanyIncorporatedInSingapore_4() throws Throwable {
		try {
	set(registerasInvestorlocators.shareholderName2, "Nagesh3");
	js_type(registerasInvestorlocators.shareholderOwnership3,"5", "share");
	set(registerasInvestorlocators.shareholderPassport3, "AP1228278728");
	set(registerasInvestorlocators.shareholderContactnumber3, pdfResultReport.testData.get("PrimaryCntctNo"));
	select(registerasInvestorlocators.shareholder_citizenship3, "Indonesia");
	waitForObj(2000);
	pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore_4","User should register as companyIncorporatedInSingapore in batumbu","Successfully registered as companyIncorporatedInSingapore in batumbu" + " ","Pass", "Y");
}catch(Exception e)
	{
		log.fatal("Unable to Homelogin" + e.getMessage());
		pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore_4",
				"User should register as companyIncorporatedInSingapore in batumbu",
				"User unable to register as companyIncorporatedInSingapore in batumbu" + e.getMessage(), "Fail", "N");
	}
	}
	public void PinjamancompanyIncorporatedInSingapore_5() throws Throwable {
		try {
	uploadFile(registerasInvestorlocators.NRICPassportFront1);
	waitForObj(8000);
	uploadFile(registerasInvestorlocators.NRICPassportBack);
	waitForObj(7000);
	uploadFile(registerasInvestorlocators.proofofResidence);
	waitForObj(7000);
	uploadFile(registerasInvestorlocators.memoandArticles);
	waitForObj(7000);
	uploadFile(registerasInvestorlocators.acraBizfile);
	waitForObj(7000);
	uploadFile(registerasInvestorlocators.actupload6);
	waitForObj(7000);
	uploadFile(registerasInvestorlocators.actupload7);
	waitForObj(7000);
	click(registerasInvestorlocators.acknowledge1);
	click(registerasInvestorlocators.acknowledge);
	click(registerasInvestorlocators.termsandCondition);
	waitForObj(2000);
	click(registerasInvestorlocators.Secondcheckbox);
	click(registerasInvestorlocators.esignatureAgree);
	waitForObj(2000);
	click(registerasInvestorlocators.registerUs);
	click(registerasInvestorlocators.agree);
	waitForObj(2000);
	pageDown();
	click(registerasInvestorlocators.submit);
	waitForObj(2000);
	click(registerasInvestorlocators.confirmMSG);

	waitForObj(3000);
	pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore_5","User should register as companyIncorporatedInSingapore in batumbu","Successfully registered as companyIncorporatedInSingapore in batumbu" + " ","Pass", "Y");
}catch(Exception e)
	{
		log.fatal("Unable to Homelogin" + e.getMessage());
		pdfResultReport.addStepDetails("PinjamancompanyIncorporatedInSingapore_5",
				"User should register as companyIncorporatedInSingapore in batumbu",
				"User unable to register as companyIncorporatedInSingapore in batumbu" + e.getMessage(), "Fail", "N");
	}
	}


	public void PinjamanIndonesianLegalEntity() throws Throwable {
		try {

			pageDown();
			click(registerasInvestorlocators.submit);
			// JSClick(registerasInvestorlocators.submit, "Submit Button");
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath("//h3[@class='profile-ban-txt']"))
					.getText();
			System.out.println(str);
			switchwindow(0);
			waitForObj(3000);
			pdfResultReport.addStepDetails("Register as PinjamanIndonesianLegalEntity",
					"User should register as PinjamanIndonesianLegalEntity",
					"Successfully registered as PinjamanIndonesianLegalEntity" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as PinjamanIndonesianLegalEntity",
					"User is not able to register as PinjamanIndonesianLegalEntity",
					"User unable to register as PinjamanIndonesianLegalEntity" + e.getMessage(), "Fail", "N");
		}
	}

	public void companyIncorporatedOutsideSingapore() throws Throwable {
		try {
			/*
			 * click(registerasInvestorlocators.validusMainpageLogin);
			 * set(registerasInvestorlocators.username,
			 * pdfResultReport.testData.get("UserName"));
			 * set(registerasInvestorlocators.password,
			 * pdfResultReport.testData.get("Password"));
			 * click(registerasInvestorlocators.login);
			 * click(registerasInvestorlocators.companyIncorporateInSingapore);
			 */
			set(registerasInvestorlocators.companyName2, pdfResultReport.testData.get("Companyname"));
			select(registerasInvestorlocators.title2, pdfResultReport.testData.get("Title"));
			// set(registerasInvestorlocators.companyRegisterationNumber, "1556672");
			set(registerasInvestorlocators.deedofEstb2, "1995");
			set(registerasInvestorlocators.UENNumber2, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.firstName2, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.companyAddress2, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.familyName2, "validus");

			set(registerasInvestorlocators.middleName2, pdfResultReport.testData.get("Middle Name"));
			set(registerasInvestorlocators.lastName2, pdfResultReport.testData.get("Last Name"));
			set(registerasInvestorlocators.industryDescription, pdfResultReport.testData.get("Industry Description"));
			set(registerasInvestorlocators.postalCode2, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.NRICPassportNumber2, pdfResultReport.testData.get("NRIC/Passport Number"));
			// set(registerasInvestorlocators.SICCCode, pdfResultReport.testData.get("SICC
			// (Industry) Code"));
			select(registerasInvestorlocators.Nationality2, pdfResultReport.testData.get("Nationality"));
			// set(registerasInvestorlocators.registrationDate2,
			// pdfResultReport.testData.get("Date of Registration"));
			set(registerasInvestorlocators.primaryContactNumber2,
					pdfResultReport.testData.get("Primary Contact Number"));
			set(registerasInvestorlocators.designation2, pdfResultReport.testData.get("Designation"));
			set(registerasInvestorlocators.netWorthPreviousYear2, "200000");
			set(registerasInvestorlocators.netWorthCurrentYear2, "300000");
			set(registerasInvestorlocators.managementName2, "Nithin");
			set(registerasInvestorlocators.directorPosition2, "CEO");
			set(registerasInvestorlocators.directorServingSince2, "1998");
			set(registerasInvestorlocators.directorPassport2, "passport1243");
			set(registerasInvestorlocators.directorCitizenship2, "yestest");
			set(registerasInvestorlocators.directorContactNo2, pdfResultReport.testData.get("Primary Contact Number"));
			set(registerasInvestorlocators.shareholderName22, "Nithin");
			set(registerasInvestorlocators.shareholderOwnership22, "30");
			set(registerasInvestorlocators.shareholderPassport2, "pasport1234");
			set(registerasInvestorlocators.shareholderCitizenship2, "yestest");
			set(registerasInvestorlocators.shareholderContactNo2,
					pdfResultReport.testData.get("Primary Contact Number"));

			// pageDown();
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.NRICPassportFront2);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.NRICPassportBack2);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.certificateofIncorporation);
			waitForObj(5000);
			uploadFile(registerasInvestorlocators.memoandArticles2);
			waitForObj(5000);
			uploadFile(registerasInvestorlocators.proofofResidence2);
			waitForObj(5000);
			uploadFile(registerasInvestorlocators.optionalFile2);
			waitForObj(4000);
			click(registerasInvestorlocators.agree2);
			click(registerasInvestorlocators.acknowledge2);
			click(registerasInvestorlocators.termsandCondition2);
			click(registerasInvestorlocators.registerUs2);
			click(registerasInvestorlocators.registerUs22);
			waitForObj(2000);
			click(registerasInvestorlocators.submit2);
			waitForObj(3000);
			pdfResultReport.addStepDetails("Register as companyIncorporatedOutsideSingapore",
					"User should register as companyIncorporatedOutsideSingapore",
					"Successfully registered as companyIncorporatedOutsideSingapore" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as companyIncorporatedOutsideSingapore",
					"User is not able to register as companyIncorporatedOutsideSingapore",
					"Unable to register as companyIncorporatedOutsideSingapore" + e.getMessage(), "Fail", "N");
		}
	}

	public void PinjamanCompanyIncorporatedOutsideSingapore() throws Throwable {
		try {
			/*
			 * click(registerasInvestorlocators.validusMainpageLogin);
			 * set(registerasInvestorlocators.username,
			 * pdfResultReport.testData.get("UserName"));
			 * set(registerasInvestorlocators.password,
			 * pdfResultReport.testData.get("Password"));
			 * click(registerasInvestorlocators.login);
			 * click(registerasInvestorlocators.companyIncorporateInSingapore);
			 */
			set(registerasInvestorlocators.companyName2, pdfResultReport.testData.get("Companyname"));
			select(registerasInvestorlocators.title2, pdfResultReport.testData.get("Title"));
			set(registerasInvestorlocators.UENNumber2, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.firstName2, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.companyAddress2, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.middleName2, pdfResultReport.testData.get("Middle Name"));
			set(registerasInvestorlocators.lastName2, pdfResultReport.testData.get("Last Name"));
			set(registerasInvestorlocators.deedofEstb2, pdfResultReport.testData.get("DeedOfEstd"));
			set(registerasInvestorlocators.industryDescription, pdfResultReport.testData.get("Industry Description"));
			set(registerasInvestorlocators.postalCode2, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.NRICPassportNumber2, pdfResultReport.testData.get("NRIC/Passport Number"));
			// set(registerasInvestorlocators.SICCCode, pdfResultReport.testData.get("SICC
			// (Industry) Code"));
			select(registerasInvestorlocators.Nationality2, pdfResultReport.testData.get("Nationality"));
			// set(registerasInvestorlocators.registrationDate2,
			// pdfResultReport.testData.get("Date of Registration"));
			set(registerasInvestorlocators.primaryConatctNo2, pdfResultReport.testData.get("PrimaryCntctNo"));
			set(registerasInvestorlocators.designationInv2, pdfResultReport.testData.get("Designation"));
			// set(registerasInvestorlocators.primaryContactNumber2,
			// pdfResultReport.testData.get("Primary Contact Number"));
			// set(registerasInvestorlocators.designation2,
			// pdfResultReport.testData.get("Designation"));
			// pageDown();
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.NRICPassportFront2);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.NRICPassportBack2);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.certificateofIncorporation);
			waitForObj(5000);
			uploadFile(registerasInvestorlocators.memoandArticles2);
			waitForObj(5000);
			uploadFile(registerasInvestorlocators.proofofResidence2);
			waitForObj(5000);
			uploadFile(registerasInvestorlocators.optionalFile2);
			waitForObj(4000);
			click(registerasInvestorlocators.agree2);
			click(registerasInvestorlocators.acknowledge2);
			click(registerasInvestorlocators.termsandCondition2);
			click(registerasInvestorlocators.registerUs2);
			click(registerasInvestorlocators.submit2);
			waitForObj(3000);
			pdfResultReport.addStepDetails("Register as companyIncorporatedOutsideSingapore",
					"User should register as companyIncorporatedOutsideSingapore",
					"Successfully registered as companyIncorporatedOutsideSingapore" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as companyIncorporatedOutsideSingapore",
					"User is not able to register as companyIncorporatedOutsideSingapore",
					"Unable to register as companyIncorporatedOutsideSingapore" + e.getMessage(), "Fail", "N");
		}
	}

	public void individualInvestorSingaporeCitizen() throws Throwable {
		try {

			select(registerasInvestorlocators.title3, pdfResultReport.testData.get("Title"));
			set(registerasInvestorlocators.firstName3, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.Address, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.middleName3, pdfResultReport.testData.get("Middle Name"));
			set(registerasInvestorlocators.lastName3, pdfResultReport.testData.get("Last Name"));
			set(registerasInvestorlocators.postalCode3, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.NRICPassportNumber3, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.networthPreviousyear, "300000");
			set(registerasInvestorlocators.networthCurrentyear, "600000");
			// pageDown();
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.NRICPassportFront3);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.NRICPassportBack3);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.proofofResidence3);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.optionalFile3);
			waitForObj(4000);
			click(registerasInvestorlocators.agree3);
			click(registerasInvestorlocators.acknowledge3);
			click(registerasInvestorlocators.termsandCondition3);
			click(registerasInvestorlocators.registerUs3);
			click(registerasInvestorlocators.registerUs33);
			click(registerasInvestorlocators.submit3);
			/*
			 * waitForElement(registerasInvestorlocators.applicationreviewmessage, 60);
			 * String str = text(registerasInvestorlocators.applicationreviewmessage);
			 * System.out.println(str); switchwindow(0);
			 */
			waitForObj(3000);
			pdfResultReport.addStepDetails("Register as individualInvestorSingaporeCitizen",
					"User should register as individualInvestorSingaporeCitizen",
					"Successfully registered as individualInvestorSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as individualInvestorSingaporeCitizen",
					"User is not able to register as individualInvestorSingaporeCitizen",
					"Unable to register as individualInvestorSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}

	public void individualInvestorNonSingaporeCitizen() throws Throwable {
		try {
			select(registerasInvestorlocators.title4, pdfResultReport.testData.get("Title"));
			set(registerasInvestorlocators.firstName4, pdfResultReport.testData.get("First Name"));
			select(registerasInvestorlocators.Nationality3, pdfResultReport.testData.get("Nationality"));
			set(registerasInvestorlocators.Address4, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.middleName4, pdfResultReport.testData.get("Middle Name"));
			set(registerasInvestorlocators.lastName4, pdfResultReport.testData.get("Last Name"));
			set(registerasInvestorlocators.postalCode4, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.NRICPassportNumber4, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.netWorthPreviousYear6, "300000");
			set(registerasInvestorlocators.netWorthCurrentYear6, "400000");
			// pageDown();
			uploadFile(registerasInvestorlocators.NRICPassportFront4);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.NRICPassportBack4);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.proofofResidence4);
			waitForObj(4000);
			uploadFile(registerasInvestorlocators.optionalFile4);
			waitForObj(4000);
			click(registerasInvestorlocators.agree4);
			click(registerasInvestorlocators.acknowledge4);
			click(registerasInvestorlocators.termsandCondition4);
			click(registerasInvestorlocators.registerUs4);
			click(registerasInvestorlocators.registerUs44);
			click(registerasInvestorlocators.submit4);
			/*
			 * String str = text(registerasInvestorlocators.successmsg);
			 * System.out.println(str);
			 */
			/*
			 * waitForElement(registerasInvestorlocators.applicationreviewmessage, 60);
			 * String str = text(registerasInvestorlocators.applicationreviewmessage);
			 * System.out.println(str); switchwindow(0);
			 */
			waitForObj(5000);
			pdfResultReport.addStepDetails("Register as individualInvestorNonSingaporeCitizen",
					"User should register as individualInvestorNonSingaporeCitizen",
					"Successfully registered as individualInvestorNonSingaporeCitizen" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as individualInvestorNonSingaporeCitizen",
					"User is not able to register as individualInvestorNonSingaporeCitizen",
					"Unable to register as individualInvestorNonSingaporeCitizen" + e.getMessage(), "Fail", "N");
		}
	}

	public void investorstatus() throws Exception {
		String str = text(registerasInvestorlocators.investorstatus);
		System.out.println(str);
	}

	public void eSignatureVerification() throws Exception {

		try {
			waitForElement(registerasInvestorlocators.esignatureClickhere, 10);
			click(registerasInvestorlocators.esignatureClickhere);
			click(registerasInvestorlocators.firstcheckbox);
			Thread.sleep(2000);
			click(registerasInvestorlocators.Secondcheckbox);
			click(registerasInvestorlocators.esignatureAgree);
			Thread.sleep(2000);
			try {

				List<WebElement> frames = ThreadLocalWebdriver.getDriver().findElements(By.tagName("iframe"));
				System.out.println(frames.size());
				switchframe(frames.get(0));
				click(registerasInvestorlocators.captcha);
				switchToDefaultFrame();
			} catch (Exception e) {
				System.out.println("Secondcheckbox is not available");
			}

			click(registerasInvestorlocators.esignatureSubmit);
			Thread.sleep(2000);

			pdfResultReport.addStepDetails("eSignatureVerification",
					"Application should verify the eSignatureVerification",
					"Successfully verified the eSignatureVerification", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("eSignatureVerification",
					"Application should verify the eSignatureVerification",
					"Unable to verify the eSignatureVerification ::" + e.getMessage(), "Fail", "N");
		}
	}

	public void bankDetails() throws Exception {

		try {
			// click(registerasInvestorlocators.bank);
			// Thread.sleep(2000);
			// click(registerasInvestorlocators.edit);
			Thread.sleep(2000);
			selectbyIndex(registerasInvestorlocators.AccountOwner, 1);
			set(registerasInvestorlocators.AccountNumber, pdfResultReport.testData.get("Account Number"));
			set(registerasInvestorlocators.BankName, pdfResultReport.testData.get("Bank Name"));
			set(registerasInvestorlocators.BranchCode, pdfResultReport.testData.get("Branch Code"));
			pdfResultReport.addStepDetails("bankDetails", "Application should enter the details in bank page",
					"Successfully entered the details in bank page", "Pass", "Y");
			click(registerasInvestorlocators.submitbank);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("bankDetails", "Application should enter the details in bank page",
					"Unable to enter the details in bank page ::" + e.getMessage(), "Fail", "N");
		}
	}

	public void investerFund() throws Exception {

		try {

			/*
			 * String str = text(registerasInvestorlocators.investorstatus);
			 * System.out.println(str);
			 */
			click(registerasInvestorlocators.platform);
			click(registerasInvestorlocators.allLivefacilities);
			String strn = text(registerasInvestorlocators.availableFunds);
			System.out.println("investerFund AVailble funds ::" + strn);
			waitForObj(3000);

			pdfResultReport.addStepDetails("Investorfund", "User should able to do fund",
					"Successfully able to fund to SME" + " ", "Pass", "Y");
			// switchwindow(0);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Investorfund", "User should able to do fund",
					"Unable to fund to SME" + e.getMessage(), "Fail", "N");
		}
	}

	public void accountSummary() throws Exception {
		try {
			click(registerasInvestorlocators.accountSummary);
			click(registerasInvestorlocators.accountSummaryArrow);
			List<WebElement> allData = ThreadLocalWebdriver.getDriver()
					.findElements(registerasInvestorlocators.accountSummaryDetailsList);
			System.out.println(allData.size());
			FundsAvailableForLending = allData.get(0).getText();
			System.out.println("FundsAvailableForLending :::" + FundsAvailableForLending);

			CommitmentOfLending = allData.get(1).getText();
			System.out.println("CommitmentOfLending :::" + CommitmentOfLending);

			AmountRequestedForWithdrawal = allData.get(2).getText();
			System.out.println("AmountRequestedForWithdrawal :::" + AmountRequestedForWithdrawal);

			LoanPortfolio = allData.get(3).getText();
			System.out.println("LoanPortfolio :::" + LoanPortfolio);

			NetReturn = allData.get(4).getText();
			System.out.println("NetReturn :::" + NetReturn);

			TheAmountOfFundsToDateIs = allData.get(10).getText();
			System.out.println("TheAmountOfFundsToDateIs :::" + TheAmountOfFundsToDateIs);

			TheAmountOfFundsWithdrawnToDateIs = allData.get(11).getText();
			System.out.println("TheAmountOfFundsWithdrawnToDateIs :::" + TheAmountOfFundsWithdrawnToDateIs);

			pdfResultReport.addStepDetails("accountSummary",
					"Application should display the account details in accountSummary page",
					"Successfully displayed the account details in accountSummary page" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("accountSummary",
					"Application should display the account details in accountSummary page",
					"Unable to display the account details in accountSummary page" + e.getMessage(), "Fail", "N");
		}
	}

	public void allLiveFecilities_Fund() throws Throwable {
		try {

			click(registerasInvestorlocators.allLivefacilities);
			waitForObj(3000);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_MINUS);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_MINUS);
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_MINUS);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_MINUS);
			waitForObj(2000);
			// mouseOver(registerasInvestorlocators.danai);
			// click(registerasInvestorlocators.danai);
			Screen s = new Screen();
			s.click(System.getProperty("user.dir").replace("\\", "/") + "/MediaFiles/Danai.png");
			waitForObj(2000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_PLUS);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_PLUS);
			Thread.sleep(2000);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_PLUS);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_PLUS);
			waitForObj(2000);
			String amtLeft = text(registerasInvestorlocators.amtLeft);
			System.out.println(amtLeft);
			waitForObj(1000);
			String availableFunds_AllLiveFecilities = text(registerasInvestorlocators.availableFunds_AllLiveFecilities);
			System.out.println("availableFunds_AllLiveFecilities ::" + availableFunds_AllLiveFecilities);
			set(registerasInvestorlocators.investmentAmount, "1000");
			pdfResultReport.addStepDetails("allLiveFecilities_Fund", "Application should fund the amount to borrower",
					"Successfully able to fund he amount to borrower and available is :"
							+ availableFunds_AllLiveFecilities,
					"Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("allLiveFecilities_Fund", "Application should fund the amount to borrower",
					"Unable to fund he amount to borrower" + e.getMessage(), "Fail", "N");
		}
	}

	public void allLiveFecilities_Confirm() throws Throwable {
		try {

			click(registerasInvestorlocators.fundamtclose);
			try {
				click(registerasInvestorlocators.fundamtclose2);
				System.out.println("Fund amount close2 is available");
			} catch (Exception e) {
				System.out.println("Fund amount close2 is not available");
			}
			try {
				click(registerasInvestorlocators.loanProvisionchkbx1);
				click(registerasInvestorlocators.loanProvisionchkbx2);
				click(registerasInvestorlocators.loanProvisionchkbx3);
				click(registerasInvestorlocators.loanProvisionchkbx4);
				click(registerasInvestorlocators.confirm);
				System.out.println("Check boxes are available");
			} catch (Exception e) {
				System.out.println("Check boxes not available");
			}
			// waitForObj(5000);
			waitForElement(registerasInvestorlocators.ok, 10);
			JSClick(registerasInvestorlocators.ok, "Ok in Allfecilities");
			waitForObj(3000);
			pdfResultReport.addStepDetails("allLiveFecilities_Confirm",
					"Application should fund the amount to borrower",
					"Successfully able to fund he amount to borrower" + " ", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("allLiveFecilities_Confirm ::" + e.getMessage());
			pdfResultReport.addStepDetails("allLiveFecilities_Confirm",
					"Application should fund the amount to borrower",
					"Unable to fund he amount to borrower" + e.getMessage(), "Fail", "N");
		}
	}

	public void withdrawal() throws Exception {
		try {
			click(registerasInvestorlocators.withDraw);
			waitForObj(2000);
			waitForElement(registerasInvestorlocators.pullAmount, 10);
			set(registerasInvestorlocators.pullAmount, "1000");
			waitForElement(registerasInvestorlocators.withDrawSubmit, 10);
			String withDrawalBalance = text(registerasInvestorlocators.withDrawalBalance);
			System.out.println("withDrawalBalance ::" + withDrawalBalance);
			pdfResultReport.addStepDetails("withdrawal", "Application should withdra the amount from Invester",
					"Successfully able to withdra the amount from Invester and balance is :" + withDrawalBalance,
					"Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("withdrawal", "Application should withdra the amount from Invester",
					"Unable to withdra the amount from Invester" + e.getMessage(), "Fail", "N");
		}
	}

	public void withdrawalConfirm() throws Exception {
		try {
			click(registerasInvestorlocators.withDrawSubmit);
			waitForElement(registerasInvestorlocators.withDrawConfirm, 10);
			click(registerasInvestorlocators.withDrawConfirm);
			waitForObj(5000);
			try {
				waitForElement(registerasInvestorlocators.withDrawClosed, 10);

				click(registerasInvestorlocators.withDrawClosed);
			} catch (Exception e) {
				System.out.println("Withdraw closed/ Try again is not available");
			}

			pdfResultReport.addStepDetails("withdrawalConfirm", "Application should confirm the withdrawal",
					"Successfully confirmed the withdrawal", "Pass", "Y");
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("withdrawalConfirm", "Application should confirm the withdrawal",
					"Unable to confirm the withdrawal" + e.getMessage(), "Fail", "N");
		}
	}

	public void registeringasSME_Individual(int no) throws Throwable {
		try {
			Thread.sleep(1000);
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
            selectbyIndex(registerasInvestorlocators.entityType, no);
			selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"));
			click(registerasInvestorlocators.next1);
			
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			//js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),"NRIC No");
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			click(registerasInvestorlocators.next2);
		
			waitForObj(8000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(8000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(8000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(8000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(8000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(8000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(8000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile7);
		    waitForObj(8000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile8);
		    waitForObj(8000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile9);
			waitForElement(registerasInvestorlocators.termsandCondition, 20);
			click(registerasInvestorlocators.termsandConditionSME);
			click(registerasInvestorlocators.submit);
			waitForElement(registerasInvestorlocators.applyForLoan, 20);
			waitForObj(18000);
			click(registerasInvestorlocators.applyForLoan);
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			
			waitForObj(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}

	public void registeringasSME_LLP(String value) throws Throwable {
		try {
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
            select(registerasInvestorlocators.entityType, value);
			selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"));
			click(registerasInvestorlocators.next1);
			
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			//js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),"NRIC No");
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			click(registerasInvestorlocators.next2);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile7);
		    waitForObj(5000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile8);
		    waitForObj(5000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile9);
			waitForElement(registerasInvestorlocators.termsandCondition, 20);
			click(registerasInvestorlocators.termsandConditionSME);
			waitForObj(2000);
			click(registerasInvestorlocators.submit);
			waitForObj(20000);
			click(registerasInvestorlocators.applyForLoan);

			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			click(registerasInvestorlocators.submit);
			//waitForObj(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}
	public void registeringasSME_LLC(String value) throws Throwable {
		try {
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
            select(registerasInvestorlocators.entityType, value);
			selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			
			/*set(registerasInvestorlocators.district, pdfResultReport.testData.get("District"));
			set(registerasInvestorlocators.countyTown, pdfResultReport.testData.get("County"));
			set(registerasInvestorlocators.provinvce, pdfResultReport.testData.get("Province"));*/
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"));
			click(registerasInvestorlocators.next1);
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			//js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),"NRIC No");
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			click(registerasInvestorlocators.next2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile7);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile8);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile9);
			waitForElement(registerasInvestorlocators.termsandCondition, 20);
			click(registerasInvestorlocators.termsandConditionSME);
			waitForObj(10000);
			click(registerasInvestorlocators.submit);
			waitForObj(20000);
			click(registerasInvestorlocators.applyForLoan);
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			click(registerasInvestorlocators.submit);
			waitForObj(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}
	public void registeringasSME_Partnership(int no) throws Throwable {
		try {
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
            selectbyIndex(registerasInvestorlocators.entityType, no);
			selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"));
			click(registerasInvestorlocators.next1);
			/*set(registerasInvestorlocators.district, pdfResultReport.testData.get("District"));
			set(registerasInvestorlocators.countyTown, pdfResultReport.testData.get("County"));
			set(registerasInvestorlocators.provinvce, pdfResultReport.testData.get("Province"));*/
			
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			//js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),"NRIC No");
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			click(registerasInvestorlocators.next2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile7);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile8);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile9);
			waitForElement(registerasInvestorlocators.termsandCondition, 20);
			click(registerasInvestorlocators.termsandConditionSME);
			waitForObj(10000);
			click(registerasInvestorlocators.submit);
			waitForObj(20000);
			click(registerasInvestorlocators.applyForLoan);
			
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			//click(registerasInvestorlocators.submit);
			waitForObj(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}
	public void registeringasSME_SoleProprietorship(int no) throws Throwable {
		try {
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
			selectbyIndex(registerasInvestorlocators.entityType, no);
            selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"));
			click(registerasInvestorlocators.next1);
			
			/*set(registerasInvestorlocators.district, pdfResultReport.testData.get("District"));
			set(registerasInvestorlocators.countyTown, pdfResultReport.testData.get("County"));
			set(registerasInvestorlocators.provinvce, pdfResultReport.testData.get("Province"));*/
			
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			//js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),"NRIC No");
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			click(registerasInvestorlocators.next2);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile7);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile8);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile9);
			waitForElement(registerasInvestorlocators.termsandCondition, 20);
			click(registerasInvestorlocators.termsandConditionSME);
			waitForObj(5000);
			click(registerasInvestorlocators.submit);
			waitForObj(20000);
			click(registerasInvestorlocators.applyForLoan);
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			click(registerasInvestorlocators.submit);
			waitForObj(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}
	public void registeringasSME_Business(int no) throws Throwable {
		try {
			Thread.sleep(1000);
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
            selectbyIndex(registerasInvestorlocators.entityType, no);
			selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"));
			click(registerasInvestorlocators.next1);
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			//js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),"NRIC No");
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			click(registerasInvestorlocators.next2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile7);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile8);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile9);
			waitForElement(registerasInvestorlocators.termsandCondition, 20);
			click(registerasInvestorlocators.termsandConditionSME);
			waitForObj(10000);
			click(registerasInvestorlocators.submit);
			waitForObj(20000);
			click(registerasInvestorlocators.applyForLoan);
			/*set(registerasInvestorlocators.district, pdfResultReport.testData.get("District"));
			set(registerasInvestorlocators.countyTown, pdfResultReport.testData.get("County"));
			set(registerasInvestorlocators.provinvce, pdfResultReport.testData.get("Province"));
			selectbyIndex(registerasInvestorlocators.title, 1);
			
			set(registerasInvestorlocators.middleName, pdfResultReport.testData.get("Middle Name"));
			
			
			
			
		//	js_type(registerasInvestorlocators.homeaddress, "Hyderabad", "HomeAddress");
		//	js_type(registerasInvestorlocators.emergencycontact, "1234567", "emergencycontact");
			uploadFile(registerasInvestorlocators.uploadFile1,"passportfront");
			waitForObj(10000);
			click(registerasInvestorlocators.agreeSME);
			click(registerasInvestorlocators.acknowledgeSME);
			
		//	click(registerasInvestorlocators.termsandConditionSME1);*/

			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			
			waitForObj(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}

	public void registeringasSME_Individual1(int no) throws Throwable {
		try {
			Thread.sleep(1000);
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));
            selectbyIndex(registerasInvestorlocators.entityType, no);
			selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"));
			click(registerasInvestorlocators.next1);
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			//js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),"NRIC No");
			set(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			click(registerasInvestorlocators.next2);
			click(registerasInvestorlocators.cvf);
			selectbyIndex(registerasInvestorlocators.buyersCompanyName, 16);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(10000);
			/*singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile7);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile8);
		    waitForObj(10000);
		    singapore_VUE_Components.uploadFile(registerasInvestorlocators.uploadFile9);*/
			waitForElement(registerasInvestorlocators.termsandCondition, 20);
			click(registerasInvestorlocators.termsandConditionSME);
			waitForObj(10000);
			click(registerasInvestorlocators.submit5);
			waitForObj(20000);
			click(registerasInvestorlocators.applyForLoan);
			/*set(registerasInvestorlocators.district, pdfResultReport.testData.get("District"));
			set(registerasInvestorlocators.countyTown, pdfResultReport.testData.get("County"));
			set(registerasInvestorlocators.provinvce, pdfResultReport.testData.get("Province"));
			selectbyIndex(registerasInvestorlocators.title, 1);
			
			set(registerasInvestorlocators.middleName, pdfResultReport.testData.get("Middle Name"));
			
			
			
			
		//	js_type(registerasInvestorlocators.homeaddress, "Hyderabad", "HomeAddress");
		//	js_type(registerasInvestorlocators.emergencycontact, "1234567", "emergencycontact");
			uploadFile(registerasInvestorlocators.uploadFile1,"passportfront");
			waitForObj(10000);
			click(registerasInvestorlocators.agreeSME);
			click(registerasInvestorlocators.acknowledgeSME);
			
		//	click(registerasInvestorlocators.termsandConditionSME1);*/

			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			
			waitForObj(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}
	
	public void registeringasSME_PDUD(int no) throws Throwable {
		try {
			set(registerasInvestorlocators.companyName, pdfResultReport.testData.get("Companyname"));

			selectbyIndex(registerasInvestorlocators.entityType, no);
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.district, pdfResultReport.testData.get("District"));
			set(registerasInvestorlocators.countyTown, pdfResultReport.testData.get("County"));
			set(registerasInvestorlocators.provinvce, pdfResultReport.testData.get("Province"));
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			selectbyIndex(registerasInvestorlocators.title, 1);
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.middleName, pdfResultReport.testData.get("Middle Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),
					"NRIC No");
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			js_type(registerasInvestorlocators.homeaddress, "Hyderabad", "HomeAddress");
			js_type(registerasInvestorlocators.emergencycontact, "1234567", "emergencycontact");
			uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(20000);
			uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile7);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile9);
			waitForObj(20000);

			waitForElement(registerasInvestorlocators.agreeSME, 20);
			click(registerasInvestorlocators.agreeSME);
			click(registerasInvestorlocators.acknowledgeSME);
			click(registerasInvestorlocators.termsandConditionSME);
			click(registerasInvestorlocators.termsandConditionSME1);

			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			click(registerasInvestorlocators.submit);
			waitForObj(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}

	public void registeringasSME_LLC(int no) throws Throwable {
		try {
			// set(registerasInvestorlocators.companyName,
			// pdfResultReport.testData.get("Companyname"));
			set(registerasInvestorlocators.companyName, emailIDInvestor1);

			selectbyIndex(registerasInvestorlocators.entityType, no);
			Thread.sleep(1000);
			set(registerasInvestorlocators.UENNumber, pdfResultReport.testData.get("UEN Number"));
			selectbyIndex(registerasInvestorlocators.companySector, 1);
			set(registerasInvestorlocators.SMECompanyAddress, pdfResultReport.testData.get("Registered Address"));
			set(registerasInvestorlocators.district, pdfResultReport.testData.get("District"));
			set(registerasInvestorlocators.countyTown, pdfResultReport.testData.get("County"));
			set(registerasInvestorlocators.provinvce, pdfResultReport.testData.get("Province"));
			set(registerasInvestorlocators.SMECompanyPostalCode, pdfResultReport.testData.get("Postal code"));
			selectbyIndex(registerasInvestorlocators.title, 1);
			set(registerasInvestorlocators.firstName, pdfResultReport.testData.get("First Name"));
			set(registerasInvestorlocators.middleName, pdfResultReport.testData.get("Middle Name"));
			set(registerasInvestorlocators.lastName, pdfResultReport.testData.get("Last Name"));
			js_type(registerasInvestorlocators.NRICPassportNumber, pdfResultReport.testData.get("NRIC/Passport Number"),
					"NRIC No");
			set(registerasInvestorlocators.SMEDesignation, pdfResultReport.testData.get("Designation"));
			selectbyIndex(registerasInvestorlocators.aboutValidus, 1);
			js_type(registerasInvestorlocators.homeaddress, "Hyderabad", "HomeAddress");
			js_type(registerasInvestorlocators.emergencycontact, "1234567", "emergencycontact");

			set(registerasInvestorlocators.noOfestablishmentDeed,
					pdfResultReport.testData.get("NoOfDeedEstablishment"));
			click(registerasInvestorlocators.dateOfEstablishment);
			waitForObj(1000);
			click(registerasInvestorlocators.dayOfEstablishmentDay);
			set(registerasInvestorlocators.managementNameLLC, pdfResultReport.testData.get("ManagementNameLLC"));
			set(registerasInvestorlocators.position, pdfResultReport.testData.get("Position"));
			click(registerasInvestorlocators.servingSince);
			waitForObj(1000);
			click(registerasInvestorlocators.dayservingSince);
			set(registerasInvestorlocators.shareholderNameLLC, pdfResultReport.testData.get("shareholderNameLLC"));
			js_type(registerasInvestorlocators.Percentageownership, pdfResultReport.testData.get("Percentageownership"),
					"Percentageownership");

			uploadFile(registerasInvestorlocators.uploadFile1);
			waitForObj(20000);
			uploadFile(registerasInvestorlocators.uploadFile2);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile3);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile4);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile5);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile6);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.uploadFile7);
			waitForObj(10000);

			uploadFile(registerasInvestorlocators.uploadFile8);
			waitForObj(15000);

			uploadFile(registerasInvestorlocators.uploadFile9);
			waitForObj(20000);

			waitForElement(registerasInvestorlocators.agreeSME, 20);
			click(registerasInvestorlocators.agreeSME);
			click(registerasInvestorlocators.acknowledgeSME);
			click(registerasInvestorlocators.termsandConditionSME);
			// click(registerasInvestorlocators.termsandConditionSME1);

			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Successfully registered as SME" + " ", "Pass", "Y");
			click(registerasInvestorlocators.submit);
			waitForObj(2000);
			click(registerasInvestorlocators.smeConfirm);

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("registeringasSME", "User should register as SME",
					"Unable to register as Investor" + e.getMessage(), "Fail", "N");
		}
	}

	public void bankDetails_SME() throws Exception {
		try {
			//Thread.sleep(2000);
			set(registerasInvestorlocators.accountNumber, pdfResultReport.testData.get("Account Number"));
			set(registerasInvestorlocators.bankName, pdfResultReport.testData.get("Bank Name"));
			//Thread.sleep(1000);
			pdfResultReport.addStepDetails("batumbuBank_SME", "Application should allow the user to enter bank details",
					"Successfully entered the details in bank details as SME" + " ", "Pass", "Y");
			click(registerasInvestorlocators.next3);
			

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("batumbuBank_SME", "Application should allow the user to enter bank details",
					"Unable to enter the details in bank details as SME" + e.getMessage(), "Fail", "N");
		}

	}
	public void application() throws Exception {
		try {
			set(registerasInvestorlocators.fullName, pdfResultReport.testData.get("FamilyName"));
			Thread.sleep(1000);
			selectbyIndex(registerasInvestorlocators.residentialStatus1, 2);
			set(registerasInvestorlocators.NRICPassport, pdfResultReport.testData.get("NRIC/Passport Number"));
			set(registerasInvestorlocators.emailID, pdfResultReport.testData.get("EmailIDInvestor"));
			set(registerasInvestorlocators.mobileNumber, pdfResultReport.testData.get("PrimaryCntctNo"));
			click(registerasInvestorlocators.next4);
			Thread.sleep(5000);
			pdfResultReport.addStepDetails("batumbuBank_SME", "Application should allow the user to enter bank details",
					"Successfully entered the details in bank details as SME" + " ", "Pass", "Y");
			
			

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("batumbuBank_SME", "Application should allow the user to enter bank details",
					"Unable to enter the details in bank details as SME" + e.getMessage(), "Fail", "N");
		}

	}
	public void batumbuBank_SME_LLC() throws Exception {
		try {
			waitForElement(registerasInvestorlocators.accountOwner, 15);
//	set(registerasInvestorlocators.accountOwner, pdfResultReport.testData.get("Account Owner"));
			set(registerasInvestorlocators.bankName, pdfResultReport.testData.get("Bank Name"));
			set(registerasInvestorlocators.accountNumber, pdfResultReport.testData.get("Account Number"));
			set(registerasInvestorlocators.branchCode, pdfResultReport.testData.get("Branch Code"));
			Thread.sleep(2000);
			click(registerasInvestorlocators.next);
			pdfResultReport.addStepDetails("batumbuBank_SME", "Application should allow the user to enter bank details",
					"Successfully entered the details in bank details as SME" + " ", "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("batumbuBank_SME", "Application should allow the user to enter bank details",
					"Unable to enter the details in bank details as SME" + e.getMessage(), "Fail", "N");
		}

	}

	public void invoiceFinancingSME() throws Throwable {
		try {
            Thread.sleep(3000);
			click(registerasInvestorlocators.invoiceFinancing);
			Thread.sleep(1000);
			js_type(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"),
					"Requested amount by SME");
			set(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"));
			//click(By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[1]/div[2]/input"));
			//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[1]/div[2]/input")).sendKeys("5000");
			Thread.sleep(1000);
			click(registerasInvestorlocators.tenureRequired);
			selectbyIndex(registerasInvestorlocators.tenureRequired, 1);
			
			/*js_type(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"),
		       "Employees");
			js_type(registerasInvestorlocators.annualRevenuepreviousyear,
					pdfResultReport.testData.get("Annual Revenue Previous year"), "Annual Revenue Previous year");
			js_type(registerasInvestorlocators.annualRevenueLatestyear,
					pdfResultReport.testData.get("Annual Revenue latest year"), "Annual Revenue latest year");
			js_type(registerasInvestorlocators.netProfitLossPreviousyear,
					pdfResultReport.testData.get("Net Profit loss previous year"), "Net Profit loss previous year");
			js_type(registerasInvestorlocators.netProfitLossLatestyear,
					pdfResultReport.testData.get("Net Profit loss latest year"), "Net Profit loss latest year");
			js_type(registerasInvestorlocators.personalGuarantor1,
					pdfResultReport.testData.get("Name of Personal Guarantor"), "Name of Personal Guarantor");
			selectbyIndex(registerasInvestorlocators.residentialStatus, 1);
			js_type(registerasInvestorlocators.annualtaxableIncomelatestyear,
					pdfResultReport.testData.get("Annual taxable income previous year"),
					"Annual taxable income previous year");
			js_type(registerasInvestorlocators.annualtaxableIncomepreviousyear,
					pdfResultReport.testData.get("Annual taxable income latest year"),
					"Annual taxable income latest year");*/
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload1);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload2);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload3);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload4);
			waitForObj(5000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload5);
			waitForObj(2000);
			
			  /*uploadFile(registerasInvestorlocators.invoiceFinancingUpload6);
			  waitForObj(10000);
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload7);
			  waitForObj(10000);
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload8);
			  waitForObj(10000);
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload9);
			  waitForObj(10000);
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload12);
			  waitForObj(10000);
			 
			click(registerasInvestorlocators.confirmAccountprofile);
			waitForObj(1000);
			click(registerasInvestorlocators.agreetobond);
			waitForObj(1000);
			click(registerasInvestorlocators.disclosureinformation);
			waitForObj(1000);
			click(registerasInvestorlocators.righttorejectafamily);
			waitForObj(1000);
			click(registerasInvestorlocators.accuracyandAuthentication);
			waitForObj(1000);
			click(registerasInvestorlocators.agree6);
			waitForObj(1000);*/
			click(registerasInvestorlocators.acceptAll);
			click(registerasInvestorlocators.endSubmit);
			//waitForElement(registerasInvestorlocators.smeMessage, 60);
			//String confirmMSG = text(registerasInvestorlocators.smeMessage);
			//System.out.println("Application submitted :"+confirmMSG);
			pdfResultReport.addStepDetails("invoiceFinancingSME", "User should register as Invoice FInancing",
					"Successfully registered as Invoice Financing" + " ", "Pass", "Y");
            Thread.sleep(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("invoiceFinancingSME", "User should register as Invoice FInancing",
					"Unable to register as Invoice FInancing" + e.getMessage(), "Fail", "N");
			
		}

	}
	public void workingCapitalSME() throws Throwable {
		try {

			click(registerasInvestorlocators.workingCapitalFinancing);
			js_type(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"),
					"loanamount");
			selectbyIndex(registerasInvestorlocators.tenureRequired, 2);
			Thread.sleep(2000);
			js_type(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"),
					"Employees");
			js_type(registerasInvestorlocators.annualRevenuepreviousyear,
					pdfResultReport.testData.get("Annual Revenue Previous year"), "Annual Revenue Previous year");
			js_type(registerasInvestorlocators.annualRevenueLatestyear,
					pdfResultReport.testData.get("Annual Revenue latest year"), "Annual Revenue latest year");
			js_type(registerasInvestorlocators.netProfitLossPreviousyear,
					pdfResultReport.testData.get("Net Profit loss previous year"), "Net Profit loss previous year");
			js_type(registerasInvestorlocators.netProfitLossLatestyear,
					pdfResultReport.testData.get("Net Profit loss latest year"), "Net Profit loss latest year");
			js_type(registerasInvestorlocators.personalGuarantor1,
					pdfResultReport.testData.get("Name of Personal Guarantor"), "Name of Personal Guarantor");
			selectbyIndex(registerasInvestorlocators.residentialStatus, 1);
			js_type(registerasInvestorlocators.annualtaxableIncomelatestyear,
					pdfResultReport.testData.get("Annual taxable income previous year"),
					"Annual taxable income previous year");
			js_type(registerasInvestorlocators.annualtaxableIncomepreviousyear,
					pdfResultReport.testData.get("Annual taxable income latest year"),
					"Annual taxable income latest year");
			uploadFile(registerasInvestorlocators.invoiceFinancingUpload1);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.invoiceFinancingUpload2);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.invoiceFinancingUpload3);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.invoiceFinancingUpload4);
			waitForObj(10000);
			uploadFile(registerasInvestorlocators.invoiceFinancingUpload5);
			waitForObj(10000);
			
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload6);
			  waitForObj(10000);
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload7);
			  waitForObj(10000);
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload8);
			  waitForObj(10000);
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload9);
			  waitForObj(10000);
			  uploadFile(registerasInvestorlocators.invoiceFinancingUpload12);
			  waitForObj(10000);
			 
			click(registerasInvestorlocators.confirmAccountprofile);
			waitForObj(1000);
			click(registerasInvestorlocators.agreetobond);
			waitForObj(1000);
			click(registerasInvestorlocators.disclosureinformation);
			waitForObj(1000);
			click(registerasInvestorlocators.righttorejectafamily);
			waitForObj(1000);
			click(registerasInvestorlocators.accuracyandAuthentication);
			waitForObj(1000);
			click(registerasInvestorlocators.agree6);
			waitForObj(1000);
			
			click(registerasInvestorlocators.submit);
			waitForElement(registerasInvestorlocators.smeMessage, 60);
			String confirmMSG = text(registerasInvestorlocators.smeMessage);
			System.out.println("Application submitted :"+confirmMSG);
			
			pdfResultReport.addStepDetails("invoiceFinancingSME", "User should register as Invoice FInancing",
					"Successfully registered as Invoice Financing" + " ", "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("invoiceFinancingSME", "User should register as Invoice FInancing",
					"Unable to register as Invoice FInancing" + e.getMessage(), "Fail", "N");
		}

	}

	public void workingCapitalFinancing_SinglePayment() throws Throwable {
		try {
            Thread.sleep(2000);
            waitForElement(registerasInvestorlocators.workingCapitalFinancing_SinglePayment, 10);
			click(registerasInvestorlocators.workingCapitalFinancing_SinglePayment);
			js_type(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"),
					"Requested amount by SME");
			set(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"));
			Thread.sleep(1000);
			click(registerasInvestorlocators.tenureRequired);
			selectbyIndex(registerasInvestorlocators.tenureRequired, 1);
			waitForObj(2000);	
			/*click(registerasInvestorlocators.confirmAccountprofile);
			waitForObj(1000);
			click(registerasInvestorlocators.agreetobond);
			waitForObj(1000);
			click(registerasInvestorlocators.disclosureinformation);
			waitForObj(1000);
			click(registerasInvestorlocators.righttorejectafamily);
			waitForObj(1000);
			click(registerasInvestorlocators.accuracyandAuthentication);
			waitForObj(1000);
			click(registerasInvestorlocators.agree6);
			waitForObj(1000);*/
			click(registerasInvestorlocators.acceptAll);
			click(registerasInvestorlocators.endSubmit);
			pdfResultReport.addStepDetails("invoiceFinancingSME", "User should register as Invoice FInancing",
					"Successfully registered as Invoice Financing" + " ", "Pass", "Y");
			//click(registerasInvestorlocators.submit);
			//waitForElement(registerasInvestorlocators.smeMessage, 60);
			//String confirmMSG = text(registerasInvestorlocators.smeMessage);
			//System.out.println("Application submitted :"+confirmMSG);
			
			

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("invoiceFinancingSME", "User should register as Invoice FInancing",
					"Unable to register as Invoice FInancing" + e.getMessage(), "Fail", "N");
		}

	}
	public void invoiceFinancingCorporateVendorFinancingSME() throws Throwable {
		try {
			//click(registerasInvestorlocators.invoicefinancingcvf);
			selectbyIndex(By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[1]/div[2]/select"), 16);
			Thread.sleep(1000);
			js_type(registerasInvestorlocators.loanamount1, pdfResultReport.testData.get("Requested amount by SME"),
					"Requested amount by SME");
			set(registerasInvestorlocators.loanamount1, pdfResultReport.testData.get("Requested amount by SME"));
			//click(By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[1]/div[2]/input"));
			//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[1]/div[2]/input")).sendKeys("5000");
			Thread.sleep(2000);
			click(registerasInvestorlocators.tenureRequired1);
			Thread.sleep(1000);
			selectbyvalue(registerasInvestorlocators.tenureRequired1, "2");
			//selectbyIndex(registerasInvestorlocators.tenureRequired1, 2);
			Thread.sleep(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload1);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload3);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload4);
			waitForObj(10000);
			click(registerasInvestorlocators.confirmAccountprofile);
			waitForObj(1000);
			click(registerasInvestorlocators.agreetobond);
			waitForObj(1000);
			click(registerasInvestorlocators.disclosureinformation);
			waitForObj(1000);
			click(registerasInvestorlocators.righttorejectafamily);
			waitForObj(1000);
			click(registerasInvestorlocators.accuracyandAuthentication);
			waitForObj(1000);
			click(registerasInvestorlocators.agree6);
			waitForObj(1000);
			
			click(registerasInvestorlocators.endSubmit1);
			Thread.sleep(4000);
			
			click(By.xpath("//button[@class='primary']"));
			pdfResultReport.addStepDetails("Register as Invoice FInancing(corporate vendor financing)",
					"User should register as Invoice FInancing(corporate vendor financing)",
					"Successfully registered as SME" + " ", "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as Invoice FInancing(corporate vendor financing)",
					"User is not able to register as Invoice FInancing(corporate vendor financing)",
					"Successfully registered as Invoice FInancing(corporate vendor financing)" + e.getMessage(), "Fail",
					"N");
		}

	}

	public void workingCapitalFinancing() throws Throwable {
		try {

			click(registerasInvestorlocators.workingcapitalFinancing);

			set(registerasInvestorlocators.requestamount, pdfResultReport.testData.get("Requested amount by SME"));
			select(registerasInvestorlocators.tenureRequired, pdfResultReport.testData.get("Tenure Required"));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get("No of employees"));
			set(registerasInvestorlocators.annualRevenuepreviousyear,
					pdfResultReport.testData.get("Annual Revenue Previous year"));
			set(registerasInvestorlocators.annualRevenueLatestyear,
					pdfResultReport.testData.get("Annual Revenue latest year"));
			set(registerasInvestorlocators.netProfitLossPreviousyear,
					pdfResultReport.testData.get("Net Profit loss previous year"));
			set(registerasInvestorlocators.netProfitLossLatestyear,
					pdfResultReport.testData.get("Net Profit loss latest year"));
			set(registerasInvestorlocators.personalGuarantor1,
					pdfResultReport.testData.get("Name of Personal Guarantor"));
			set(registerasInvestorlocators.residentialStatus, pdfResultReport.testData.get("Residential Status"));

			// pageDown();
			// uploadFile(registerasInvestorlocators.cbsfileforalldirectors);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.cbsfileforallpersonalguarantor);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.bankstatementforlast6months);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.incometaxassessment);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.incometaxassessmentdirectors);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.incometaxassessmentpersonalGuarantors);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.accountreceivablelast12months);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.lastFinancialstatements);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.yeartodatemanagement);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.optionalfilesSME1);
			waitForObj(2000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.certifiedtrueExtracts);
			waitForObj(2000);

			click(registerasInvestorlocators.confirmAccountprofile);
			click(registerasInvestorlocators.agreetobond);
			click(registerasInvestorlocators.disclosureinformation);
			click(registerasInvestorlocators.righttorejectafamily);
			click(registerasInvestorlocators.accuracyandAuthentication);
			click(registerasInvestorlocators.confirmthestatements);
			click(registerasInvestorlocators.submit);

			pdfResultReport.addStepDetails("Register as Working capital Financing",
					"User should register as Working capital Financing",
					"Successfully registered as Working capital Financing" + " ", "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as Working capital Financing",
					"User is not able to register as Working capital Financing",
					"Successfully registered as Working capital Financing" + e.getMessage(), "Fail", "N");
		}
	}

	public void workingCapitalFinancingSinglePayment() throws Throwable {
		try {

			click(registerasInvestorlocators.workingcapitalFinancingsinglepayment);

			set(registerasInvestorlocators.requestamount, pdfResultReport.testData.get(""));
			select(registerasInvestorlocators.tenureRequired, pdfResultReport.testData.get(""));
			set(registerasInvestorlocators.noofEmployees, pdfResultReport.testData.get(""));
			set(registerasInvestorlocators.annualRevenuepreviousyear, pdfResultReport.testData.get(" "));
			set(registerasInvestorlocators.annualRevenueLatestyear, pdfResultReport.testData.get(" "));
			set(registerasInvestorlocators.netProfitLossPreviousyear, pdfResultReport.testData.get(" "));
			set(registerasInvestorlocators.netProfitLossLatestyear, pdfResultReport.testData.get(" "));
			set(registerasInvestorlocators.personalGuarantor1, pdfResultReport.testData.get(" "));
			set(registerasInvestorlocators.residentialStatus, pdfResultReport.testData.get(" "));
			set(registerasInvestorlocators.annualtaxableIncomelatestyear, pdfResultReport.testData.get(" "));
			set(registerasInvestorlocators.annualtaxableIncomepreviousyear, pdfResultReport.testData.get(" "));

			// pageDown();
			uploadFile(registerasInvestorlocators.workContractDocument);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.workProgress);
			waitForObj(2000);
			// uploadFile(registerasInvestorlocators.cbsfileforalldirectors);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.cbsfileforallpersonalguarantor);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.bankstatementforlast6months);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.incometaxassessment);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.incometaxassessmentdirectors);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.incometaxassessmentpersonalGuarantors);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.accountreceivablelast12months);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.lastFinancialstatements);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.yeartodatemanagement);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.optionalfilesSME1);
			waitForObj(2000);
			uploadFile(registerasInvestorlocators.certifiedtrueExtracts);
			waitForObj(2000);

			click(registerasInvestorlocators.confirmAccountprofile);
			click(registerasInvestorlocators.agreetobond);
			click(registerasInvestorlocators.disclosureinformation);
			click(registerasInvestorlocators.righttorejectafamily);
			click(registerasInvestorlocators.accuracyandAuthentication);
			click(registerasInvestorlocators.confirmthestatements);
			click(registerasInvestorlocators.submit);

			pdfResultReport.addStepDetails("Register as Working capital Financing(Single Payment)",
					"User should register as Working capital Financing(Single Payment)",
					"Successfully registered as Working capital Financing(Single Payment)" + " ", "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as Working capital Financing(Single Payment)",
					"User is not able to register as Working capital Financing(Single Payment)",
					"Unable to register as Working capital Financing(Single Payment)" + e.getMessage(), "Fail", "N");
		}
	}

	public void invoiceFinancingcardfacility() throws Throwable {
		try {
            Thread.sleep(5000);
			click(registerasInvestorlocators.invoiceFinancingCardFacility);

			Thread.sleep(1000);
			js_type(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"),
					"Requested amount by SME");
			set(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"));
			
			Thread.sleep(1000);
			click(registerasInvestorlocators.tenureRequired);
			selectbyIndex(registerasInvestorlocators.tenureRequired, 1);
			Thread.sleep(2000);

			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload1);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload2);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload3);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload4);
			waitForObj(10000);
			
			click(registerasInvestorlocators.confirmAccountprofile);
			waitForObj(1000);
			click(registerasInvestorlocators.agreetobond);
			waitForObj(1000);
			click(registerasInvestorlocators.disclosureinformation);
			waitForObj(1000);
			click(registerasInvestorlocators.righttorejectafamily);
			waitForObj(1000);
			click(registerasInvestorlocators.accuracyandAuthentication);
			waitForObj(1000);
			click(registerasInvestorlocators.agree6);
			waitForObj(1000);
			
			click(registerasInvestorlocators.endSubmit);

			pdfResultReport.addStepDetails("Register as Working Invoice financing(card facility)",
					"User should register as Invoice financing(card facility)",
					"Successfully registered as Invoice financing(card facility)" + " ", "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("Register as Invoice financing(card facility)",
					"User is not able to register as Invoice financing(card facility)",
					"Unable to register as Invoice Financing(card facility)" + e.getMessage(), "Fail", "N");
		}
	}

	public void purchaseOrderFinancingSME() throws Throwable {
		try {
            Thread.sleep(5000);
			click(registerasInvestorlocators.purchaseOrderFinancing);
			Thread.sleep(1000);
			js_type(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"),
					"Requested amount by SME");
			set(registerasInvestorlocators.loanamount, pdfResultReport.testData.get("Requested amount by SME"));
			//click(By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[1]/div[2]/input"));
			//ThreadLocalWebdriver.getDriver().findElement(By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[1]/div[2]/input")).sendKeys("5000");
			Thread.sleep(1000);
			click(registerasInvestorlocators.tenureRequired);
			selectbyIndex(registerasInvestorlocators.tenureRequired, 1);
			Thread.sleep(2000);
			
					
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload1);
			waitForObj(10000);
			singapore_VUE_Components.uploadFile(registerasInvestorlocators.invoiceFinancingUpload2);
			waitForObj(10000);
			
			 
			click(registerasInvestorlocators.confirmAccountprofile);
			waitForObj(1000);
			click(registerasInvestorlocators.agreetobond);
			waitForObj(1000);
			click(registerasInvestorlocators.disclosureinformation);
			waitForObj(1000);
			click(registerasInvestorlocators.righttorejectafamily);
			waitForObj(1000);
			click(registerasInvestorlocators.accuracyandAuthentication);
			waitForObj(1000);
			click(registerasInvestorlocators.agree6);
			waitForObj(1000);
			
			click(registerasInvestorlocators.endSubmit);
			//waitForElement(registerasInvestorlocators.smeMessage, 60);
			//String confirmMSG = text(registerasInvestorlocators.smeMessage);
			//System.out.println("Application submitted :"+confirmMSG);
			pdfResultReport.addStepDetails("invoiceFinancingSME", "User should register as Invoice FInancing",
					"Successfully registered as Invoice Financing" + " ", "Pass", "Y");
            Thread.sleep(2000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("invoiceFinancingSME", "User should register as Invoice FInancing",
					"Unable to register as Invoice FInancing" + e.getMessage(), "Fail", "N");
			
		}

	}
	
	public void verifyBrokenLinksMainPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksMainPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksMainPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinkMainPage(String from, String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/");
			waitForObj(3000);
			Actions a = new Actions(ThreadLocalWebdriver.getDriver());
			a.keyDown(Keys.CONTROL).sendKeys(Keys.END).build().perform();
			List<WebElement> ele = ThreadLocalWebdriver.getDriver().findElements(By.xpath(
					"//div[@class='uk-width-1-1']/div[@class='uk-slidenav-position uk-margin']//ul[@class='uk-slider uk-grid-width-medium-1-4']/li"));
			System.out.println(ele.size());
			for (int i = 1; i <= 18; i++) {
				if (ele.get(i).isDisplayed()) {
					// waitForObj(3000);
					a.moveToElement(ThreadLocalWebdriver.getDriver().findElement(By.xpath(
							"//div[@class='uk-width-1-1']/div[@class='uk-slidenav-position uk-margin']//ul[@class='uk-slider uk-grid-width-medium-1-4']/li[@data-slide='"
									+ i + "']")))
							.build().perform();

					try {
						ThreadLocalWebdriver.getDriver().findElement(By.xpath(
								"//div[@class='uk-width-1-1']/div[@class='uk-slidenav-position uk-margin']//ul[@class='uk-slider uk-grid-width-medium-1-4']/li[@data-slide='"
										+ i + "']"))
								.click();
						/*
						 * String str1 = ThreadLocalWebdriver.getDriver().switchTo().alert().getText();
						 * System.out.println(str1); ThreadLocalWebdriver.getDriver().findElement(By.
						 * xpath("(//button[@class='uk-modal-close uk-close'])["+i+"]"));
						 */
						Thread.sleep(2000);
						Robot robot = new Robot();
						robot.keyPress(KeyEvent.VK_ESCAPE);
						robot.keyRelease(KeyEvent.VK_ESCAPE);

					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				/*
				 * String str1 = ThreadLocalWebdriver.getDriver().switchTo().alert().getText();
				 * System.out.println(str1);
				 */

				// ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//button[@class='uk-modal-close
				// uk-close'])["+i+"]"));
				/*
				 * String str =
				 * ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
				 * waitForObj(3000);
				 */
				/*
				 * if(str1.equalsIgnoreCase(expctd)) { System.out.println("Link is not broken");
				 * }else { System.out.println("Link is broken"); }
				 */
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksMainPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			// ThreadLocalWebdriver.getDriver().quit();

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksMainPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
			// ThreadLocalWebdriver.getDriver().quit();
		}

	}

	public void verifyBrokenLinksContactsPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/contact.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksContactsPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksContactsPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksIndexPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/index.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksIndexPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksContactsPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksInvest(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/invest.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksInvest",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksInvest",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyFooterlinks(String from, String URL, String loc1, String loc2, String expctd, String to)
			throws Exception {
		try {
			ThreadLocalWebdriver.getDriver().get(URL);
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			switchwindow(1);
			waitForObj(2000);
			/*
			 * String str =
			 * ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			 * waitForObj(3000);
			 * 
			 * if(str.equalsIgnoreCase(expctd)) { System.out.println("Link is not broken");
			 * }else { System.out.println("Link is broken"); }
			 */
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyFooterlinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			ThreadLocalWebdriver.getDriver().close();
			waitForObj(3000);
			switchwindow(0);
			waitForObj(3000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyFooterlinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");

			ThreadLocalWebdriver.getDriver().close();
			waitForObj(1000);
			switchwindow(0);
			waitForObj(1000);
		}
	}

	public void verifyFooterlink(String from, String URL, String loc1, String expctd, String to) throws Exception {
		try {
			ThreadLocalWebdriver.getDriver().get(URL);
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			switchwindow(1);
			waitForObj(2000);
			/*
			 * String str =
			 * ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			 * waitForObj(3000);
			 * 
			 * if(str.equalsIgnoreCase(expctd)) { System.out.println("Link is not broken");
			 * }else { System.out.println("Link is broken"); }
			 */
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyFooterlinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			ThreadLocalWebdriver.getDriver().close();
			waitForObj(3000);
			switchwindow(0);
			waitForObj(3000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyFooterlinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");

			ThreadLocalWebdriver.getDriver().close();
			waitForObj(1000);
			switchwindow(0);
			waitForObj(1000);
		}

	}

	public void verifyBrokenLinksSME(String from, String loc1, String loc2, String expctd, String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/borrow.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(5000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksSME",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksSME",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyPressBrokenLinks(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/press.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(5000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyPressBrokenLinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			// ThreadLocalWebdriver.getDriver().close();
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyPressBrokenLinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksPress(String from, String loc1, String to) throws Throwable {
		try {
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/press.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			switchwindow(1);
			waitForObj(6000);

			/*
			 * String str =
			 * ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			 * waitForObj(3000);
			 * 
			 * if(str.equalsIgnoreCase(expctd)) { System.out.println("Link is not broken");
			 * }else { System.out.println("Link is broken"); }
			 */
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksPress",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			ThreadLocalWebdriver.getDriver().close();
			switchwindow(0);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksPress",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}
	}

	public void verifyMainPageBrokenLinks(String from, String loc1, String to) throws Throwable {
		try {
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/press.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			switchwindow(1);
			waitForObj(2000);

			/*
			 * String str =
			 * ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			 * waitForObj(3000);
			 * 
			 * if(str.equalsIgnoreCase(expctd)) { System.out.println("Link is not broken");
			 * }else { System.out.println("Link is broken"); }
			 * //ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");
			 */

			pdfResultReport.addStepDetails("verifyMainPageBrokenLinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			ThreadLocalWebdriver.getDriver().close();
			switchwindow(0);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyMainPageBrokenLinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}
	}

	public void verifyBrokenLinksAbout(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/about.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			// ThreadLocalWebdriver.getDriver().switchTo().alert();
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksAbout",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksAbout",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyCompanytabBoardofAdvisors(String from, String URL, String loc1, String loc2, String expctd,
			String to) throws Throwable {

		try {
			ThreadLocalWebdriver.getDriver().get(URL);
			mouseOver(By.xpath(loc1));
			waitForObj(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//a[contains(text(),'Board of Advisors')])[1]"))
					.click();
			waitForObj(3000);
			// ThreadLocalWebdriver.getDriver().switchTo().alert();
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}

			pdfResultReport.addStepDetails("verifyCompanytabBoardofAdvisors",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			// ThreadLocalWebdriver.getDriver().close();
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyCompanytabBoardofAdvisors",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyCompanytabCarrers(String from, String URL, String loc1, String loc2, String expctd, String to)
			throws Throwable {

		try {

			ThreadLocalWebdriver.getDriver().get(URL);
			mouseOver(By.xpath(loc1));
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//a[contains(text(),'Careers')])[1]")).click();
			waitForObj(3000);
			// ThreadLocalWebdriver.getDriver().switchTo().alert();
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}

			pdfResultReport.addStepDetails("verifyCompanytabCarrers",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			// ThreadLocalWebdriver.getDriver().close();
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyCompanytabCarrers",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyCompanytabFAQ(String from, String URL, String loc1, String loc2, String expctd, String to)
			throws Exception {

		try {

			ThreadLocalWebdriver.getDriver().get(URL);
			mouseOver(By.xpath(loc1));
			waitForObj(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath("(//a[contains(text(),'FAQs')])[1]")).click();
			waitForObj(3000);
			// ThreadLocalWebdriver.getDriver().switchTo().alert();
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			pdfResultReport.addStepDetails("verifyCompanytabFAQ",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			// ThreadLocalWebdriver.getDriver().close();
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyCompanytabFAQ",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksAdvisor(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/board-of-advisors.html");
			waitForObj(3000);

			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			// ThreadLocalWebdriver.getDriver().switchTo().alert();
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksAdvisor",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksAdvisor",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksCareer(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/careers.html");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksCareer",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksCareer",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksFAQ(String from, String loc1, String loc2, String expctd, String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get("http://batumbu.com/pinjaman_bahasa_enkripsi/faq.html");
			waitForObj(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(4000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksFAQ",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksFAQ",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksLogin(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/home/login");
			waitForObj(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(4000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksLogin",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksLogin",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksResetpassword(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/home/forgot_password");
			waitForObj(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(4000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksResetpassword",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksResetpassword",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksInvestorRegister(String from, String url, String loc1, String loc2, String loc3,
			String loc4, String loc5, String expctd, String to) throws Throwable {
		try {

			ThreadLocalWebdriver.getDriver().get(url);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).click();
			waitForObj(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc4)).click();
			waitForObj(4000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc5)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksInvestorRegister",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksInvestorRegister",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyBrokenLinksSMERegister(String from, String url, String loc1, String loc2, String loc3,
			String loc4, String loc5, String expctd, String to) throws Throwable {
		try {

			ThreadLocalWebdriver.getDriver().get(url);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).click();
			waitForObj(4000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc4)).click();
			waitForObj(4000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc5)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksSMERegister",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksSMERegister",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");
		}

	}

	public void verifyPDFlinks(String from, String URL, String loc1, String loc2, String expctd, String to)
			throws Exception {
		try {
			ThreadLocalWebdriver.getDriver().get(URL);
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			switchwindow(1);
			waitForObj(2000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyFooterlinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");
			ThreadLocalWebdriver.getDriver().close();
			waitForObj(3000);
			switchwindow(0);
			waitForObj(3000);
		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyFooterlinks",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
			// pdfResultReport.addStepDetails("verifyBrokenLinksMainPage","Application
			// should verify the link in
			// :"+ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Unable to navigate to
			// the link : " +ThreadLocalWebdriver.getDriver().getCurrentUrl(),"Fail", "N");

			ThreadLocalWebdriver.getDriver().close();
			waitForObj(1000);
			switchwindow(0);
			waitForObj(1000);
		}
	}

	public void verifyBrokenLinksProfilePage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/profile");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksProfilePage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksProfilePage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksHowToStartPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/how_to_start");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksHowToStartPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksHowToStartPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksFacilitiesParticipatedPage(String from, String loc1, String loc2, String expctd,
			String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get(
					"http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/facilities_participated");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksFacilitiesParticipatedPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksFacilitiesParticipatedPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksFacilitiesParticipated(String from, String loc1, String loc2, String loc3,
			String expctd, String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver().get(
					"http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/facilities_participated");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksFacilitiesParticipated",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksFacilitiesParticipated",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksAllLiveFacilitiesPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/all_live_facilities");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksAllLiveFacilitiesPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksAllLiveFacilitiesPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksAllLiveFacilities(String from, String loc1, String loc2, String loc3, String expctd,
			String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/all_live_facilities");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksAllLiveFacilities",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksAllLiveFacilities",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksTransactionsPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/transactions");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksTransactionsPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksTransactionsPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksTransactions(String from, String loc1, String loc2, String loc3, String expctd,
			String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/transactions");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksTransactions",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksTransactions",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksFinancialSummaryPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/financial_summary");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksFinancialSummaryPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksFinancialSummaryPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksFinancialSummary(String from, String loc1, String loc2, String loc3, String expctd,
			String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/financial_summary");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksFinancialSummary",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksFinancialSummary",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksAccountSummaryPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/account_summary");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksAccountSummaryPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksAccountSummaryPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksAccountSummary(String from, String loc1, String loc2, String loc3, String expctd,
			String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/account_summary");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksHowToStart",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksHowToStart",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksProfilePagetwo(String from, String loc1, String loc2, String loc3, String expctd,
			String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/profile");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksProfilePage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksProfilePage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksBankPage(String from, String loc1, String loc2, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/bank");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksBankPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksBankPage",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksBank(String from, String loc1, String loc2, String loc3, String expctd, String to)
			throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/bank");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksBank",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksBank",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void verifyBrokenLinksHowToStart(String from, String loc1, String loc2, String loc3, String expctd,
			String to) throws Throwable {
		try {
			ThreadLocalWebdriver.getDriver()
					.get("http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/how_to_start");
			waitForObj(3000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc1)).click();
			Thread.sleep(2000);
			ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc2)).click();
			waitForObj(3000);
			String str = ThreadLocalWebdriver.getDriver().findElement(By.xpath(loc3)).getText();
			waitForObj(3000);

			if (str.equalsIgnoreCase(expctd)) {
				System.out.println("Link is not broken");
			} else {
				System.out.println("Link is broken");
			}
			// ThreadLocalWebdriver.getDriver().get("http://149.129.218.139/pinjaman/");

			pdfResultReport.addStepDetails("verifyBrokenLinksHowToStart",
					"Application should verify the links from : " + from + "-->" + to,
					"Successfully navigated to the link :" + from + "-->" + to, "Pass", "Y");

		} catch (Exception e) {
			log.fatal("Unable to Homelogin" + e.getMessage());
			pdfResultReport.addStepDetails("verifyBrokenLinksHowToStart",
					"Application should verify the links from : " + from + "-->" + to,
					"Unable to navigate to the link :" + from + "-->" + to, "Fail", "Y");
		}

	}

	public void logout() throws Throwable {
		try {
			click(registerasInvestorlocators.batumbulogout);
			Thread.sleep(2000);
			pdfResultReport.addStepDetails("logout", "User should logout from the application",
					"Successfully logout from the application" + " ", "Pass", "Y");
		} catch (Exception e3) {
			log.fatal("Unable to verify the mobile and email verification in Salesforce" + e3.getMessage());
			pdfResultReport.addStepDetails("logout", "User should logout from the application",
					"Unable to logout from the application" + e3.getMessage(), "Fail", "N");
		}

	}

}
