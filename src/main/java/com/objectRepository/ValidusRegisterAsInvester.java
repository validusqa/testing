package com.objectRepository;

import org.openqa.selenium.By;

public class ValidusRegisterAsInvester {

	public By validusMainpageLogin = By.xpath("//a[@href='http://doodlebluemobile.com/validus/validusdev/platform/index.php/home/login']");
	public By pinjamnHomepage = By.xpath("//a[contains(text(),'Login')]");
	public By resetPasswordPinajaman = By.xpath("//a[contains(text(),' Atur ulang sekarang.')]");
	public By resetPassword = By.xpath("//a[contains(text(),' Reset now.')]");
	public By resetEmailaddress = By.xpath("//input[@id = 'EMAIL_ID']");
	public By resetPasswordsubmitButton = By.xpath("//button[@type='submit']");
	public By resetpasswordfinal = By.xpath("//input[@id='Password']");
	public By resetConfirmPasswordFinal = By.xpath("//input[@id='ConfirmPassword']");
	public By resetpasswordbuttonfinal = By.xpath("//button[@type='submit']");
	public By username = By.xpath("//input[@id='Email_Id']");
	public By password = By.xpath("//input[@id='Password']");
	public By login = By.xpath("//*[@id='app']/div/div[2]/div[2]/div/div[2]/div/div[5]/button");
	public By companyIncorporateInSingapore = By.xpath("//label[@for='inlineRadio1']");
	public By companyIncorporateOutsideSingapore = By.xpath("//label[@for='inlineRadio2']");
	public By individualInvestorSingaporeCitizen  = By.xpath("//label[@for='inlineRadio3']");
	public By individualInvestorNonSingaporeCitizen  = By.xpath("//label[@for='inlineRadio4']");
	
	public By indonesianLegalEntity = By.xpath("//label[@for='inlineRadio5']");
	
	//Investor ForeignBusinessEntity locators
	public By foreignBusinessEntity = By.xpath("//label[@for='inlineRadio2']");
	public By foreignBusinessEntitycompanyName2 = By.xpath("(//input[@id='CompanyName'])[2]");
	
	
	public By companyName = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[2]/div[2]/input");
	public By companyName2 = By.xpath("(//input[@id='CompanyName'])[2]");
	public By title = By.xpath("//select[@id='Salutation']");
	public By title2 = By.xpath("(//select[@id='Salutation'])[2]");
	public By title3 = By.xpath("(//select[@id='Salutation'])[5]");
	public By title4 = By.xpath("(//select[@id='Salutation'])[6]");
	public By companyRegisterationNumber = By.xpath("(//input[@id='CompanyRegistrationNumber1'])[1]");
	public By UENNumber = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[1]/div/div[2]/input");
	public By UENNumber2 = By.xpath("(//input[@id='CompanyRegistrationNumber1'][1])");
	public By deedofEstb2 = By.xpath("(//input[@name='DeedOfEstablishment'])[2]");
	public By firstName = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[1]/div[2]/input");
	public By firstName2 = By.xpath("(//input[@id='FirstName'])[2]");
	public By firstName3 = By.xpath("(//input[@id='FirstName'])[5]");
	public By firstName4 = By.xpath("(//input[@id='FirstName'])[6]");
	public By companyAddress = By.xpath("(//input[@id='CompanyAddress'])[1]");
	public By deedofEstb = By.xpath("(//input[@name='DeedOfEstablishment'])[1]");
	public By deedofEstb2old = By.xpath("(//input[@id='DeedOfEstablishment'])[2]");
	public By primaryContactNo = By.xpath("(//input[@id='PrimaryContactNumber'])[1]");
	public By primaryConatctNo2 = By.xpath("(//input[@id='PrimaryContactNumber1'])[1]");
	public By designationInv = By.xpath("(//input[@id='Designation'])[1]");
	public By designationInv2 = By.xpath("(//input[@id='Designation'])[2]");
	public By companyAddress2 = By.xpath("//input[@id='CompanyAddress1']");
	public By familyName2 = By.xpath("(//input[@name='FamilyName'])[2]");
	public By directorPassportNo = By.xpath("(//input[@name='director_passport_no[]'])[2]");
	public By Address = By.xpath("(//input[@id='Address'])[1]");
	public By Address4 = By.xpath("(//input[@id='Address'])[2]");
	public By netWorthPreviousYear6 = By.xpath("(//input[@name='net_worth_previous_year'])[6]");
	public By netWorthCurrentYear6 = By.xpath("(//input[@name='net_worth_current_year'])[6]");
	public By netWorthPreviousYear = By.xpath("(//input[@name='net_worth_previous_year'])[1]");
	public By netWorthCurrentYear = By.xpath("(//input[@name='net_worth_current_year'])[1]");
					
	
	public By middleName = By.xpath("//input[@id='MiddleName']");
	public By familyName = By.xpath("(//input[@id='FamilyName'])[1]");
	public By nricName = By.xpath("(//input[@id='NRICNumber'])[1]");
	public By companyZipCode = By.xpath("(//input[@id='CompanyZipCode'])[1]");
	public By nationality = By.xpath("(//select[@id='Nationality'])[1]");
	public By siCC = By.xpath("(//select[@id='SICCCode'])[1]");
	public By designationPinjaman = By.xpath("(//input[@id='Designation'])[1]");
	public By primaryContactNumberPinjaman = By.xpath("(//input[@id='PrimaryContactNumber'])[1]");
	public By managementName = By.xpath("(//input[@class='managementName'])[1]");
	public By directorPosition = By.xpath("(//input[@class='director_position'])[1]");
	public By directorServingSince= By.xpath("(//input[@class='director_servingSince'])[1]");
	public By directorPassportNumber = By.xpath("(//input[@class='director_passport_no'])[1]");
	public By directorCitizenship = By.xpath("(//select[@name='director_citizenship[]'])[1]");
	
	public By shareholder_citizenship = By.xpath("(//select[@name='shareholder_citizenship[]'])[1]");
	public By shareholder_citizenship2 = By.xpath("(//select[@name='shareholder_citizenship[]'])[2]");
	public By shareholder_citizenship3 = By.xpath("(//select[@name='shareholder_citizenship[]'])[3]");
	
	public By directorContactNo = By.xpath("(//input[@class='director_contact_no'])[1]");
	public By shareholderOwnership = By.xpath("(//input[@name='shareholder_ownership[]'])[1]");
	public By shareholderPassportnumber = By.xpath("(//input[@name='shareholder_passport_no[]'])[1]");
	public By shareholderCitizenship = By.xpath("(//input[@name='shareholder_citizenship[]'])[1]");
	
	public By shareholderCitizenship_NEW = By.xpath("(//input[@name=\"shareholder_ktp_no[]\"])[1]");
	
	public By shareholderContactnumber = By.xpath("(//input[@name='shareholder_contact_no[]'])[1]");
	public By shareholderOwnership2 = By.xpath("(//input[@name='shareholder_ownership[]'])[2]");
	public By shareholderPassportNumber = By.xpath("(//input[@class='shareholder_passport_no'])[2]");
	public By shareholderCitizen1 = By.xpath("(//input[@class='shareholder_citizenship'])[1]");
	public By shareholderContactnumber1 = By.xpath("(//input[@class='shareholder_contact_no'])[2]");
	public By shareholderOwnership3 = By.xpath("(//input[@name='shareholder_ownership[]'])[3]");
	public By shareholderPassport3 = By.xpath("(//input[@class='shareholder_passport_no'])[3]");
	public By shareholderCitizenship3 = By.xpath("(//input[@class='shareholder_citizenship'])[3]");
	public By shareholderContactnumber3 = By.xpath("(//input[@class='shareholder_contact_no'])[3]");
	public By shareholderName = By.xpath("(//input[@name='shareholderName[]'])[1]");
	public By noOFShareholders = By.xpath("(//select[@name='no_of_shareholders'])[1]");
	public By  shareholderCitizenship1 = By.xpath("(//input[@name='shareholder_citizenship[]'])[2]");
	public By  shareholderCitizenship_New = By.xpath("//select[@name='shareholder_citizenship[]']");

	public By shareholderName1 = By.xpath("(//input[@name='shareholderName[]'])[2]");
		public By shareholderName2 = By.xpath("(//input[@name='shareholderName[]'])[3]");
	
	
	public By middleName2 = By.xpath("(//input[@id='MiddleName'])[2]");
	public By middleName3 = By.xpath("(//input[@id='MiddleName'])[5]");
	public By middleName4 = By.xpath("(//input[@id='MiddleName'])[6]");
    public By lastName = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[2]/div[2]/input");
    public By lastName2 = By.xpath("(//input[@id='FamilyName'])[2]");
    public By lastName3 = By.xpath("(//input[@id='FamilyName'])[5]");
    public By lastName4 = By.xpath("(//input[@id='FamilyName'])[6]");
    public By postalCode = By.xpath("//input[@id='CompanyZipCode']");
    public By postalCode2 = By.xpath("//input[@id='CompanyZipCode1']");
    public By postalCode3 = By.xpath("(//input[@id='PostalCode'])[1]");
    public By networthPreviousyear = By.xpath("(//input[@name='net_worth_previous_year'])[5]");
    public By networthCurrentyear = By.xpath("(//input[@name='net_worth_current_year'])[5]");
    public By postalCode4 = By.xpath("(//input[@id='PostalCode'])[2]");
    
    public By NRICPassportNumber = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[3]/div[2]/input");
    public By NRICPassportNumber2 = By.xpath("(//input[@id='NRICNumber'])[2]");
    public By NRICPassportNumber3 = By.xpath("(//input[@id='NRICNumber'])[5]");
    public By NRICPassportNumber4 = By.xpath("(//input[@id='NRICNumber'])[6]");
    public By homeaddress = By.xpath("//*[@id=\"homeaddress\"]");
    public By emergencycontact = By.xpath("//*[@id=\"emergencycontact\"]");
   
    //-----SME - LLC Locators
    public By noOfestablishmentDeed = By.xpath("//input[@id=\"establishmentDeedNumber\"]");
    public By dateOfEstablishment = By.xpath("//input[@id=\"deedOfEstablishment\"]");
    public By dayOfEstablishmentDay = By.xpath("//div[@class=\"datepicker-days\"]//table[@class=\" table-condensed\"]//tr//td[@class=\"day\"]");
    public By managementNameLLC = By.xpath("//input[@id=\"managementName\"]");
    public By position = By.xpath("//input[@name=\"position[]\"]");
    
    public By servingSince = By.xpath("//input[@name=\"servingSince[]\"]");
    public By dayservingSince = By.xpath("//div[@class='datepicker-days']//table[@class=' table-condensed']//tr//td[@class='day']");
    public By shareholderNameLLC = By.xpath("//input[@id=\"shareholderName\"]");
    public By Percentageownership = By.xpath("//input[@name='ownership[]']");
   // public By position = By.xpath("//input[@name=\"position[]\"]");
    
    
 
    
    public By industryDescription = By.xpath("//input[@id='IndustryDescription1']");
    
    public By SICCCode = By.xpath("//input[@id='SICCCode']");
    public By Nationality = By.xpath("//select[@id='Nationality']");
    public By Nationality2 = By.xpath("(//select[@id='Nationality'])[2]");
    public By Nationality3 = By.xpath("(//select[@id='Nationality'])[5]");
    public By registrationDate = By.xpath("//input[@id='RegistrationDate']");
    public By registrationDate2 = By.xpath("//input[@id='RegistrationDate1']");
    
    
    public By primaryContactNumber = By.xpath("//input[@id='PrimaryContactNumber']");
    public By primaryContactNumber2 = By.xpath("//input[@id='PrimaryContactNumber1']");
    
    public By designation = By.xpath("//input[@id='Designation']");
    public By designation2 = By.xpath("(//input[@id='Designation'])[2]");
    public By netWorthPreviousYear2 = By.xpath("(//input[@name='net_worth_previous_year'])[2]");
    public By netWorthCurrentYear2 = By.xpath("(//input[@name='net_worth_current_year'])[2]");
    public By managementName2 = By.xpath("(//input[@name='managementName[]'])[2]");
    public By directorPosition2 = By.xpath("(//input[@name='director_position[]'])[2]");
    public By directorServingSince2 = By.xpath("(//input[@name='director_servingSince[]'])[2]");
    public By directorPassport2 = By.xpath("(//input[@name='director_passport_no[]'])[2]");
    public By directorCitizenship2 = By.xpath("(//input[@name='director_citizenship[]'])[2]");
    public By directorContactNo2 = By.xpath("(//input[@name='director_contact_no[]'])[2]");
    public By shareholderName22 = By.xpath("(//input[@name='shareholderName[]'])[2]");
    public By shareholderOwnership22 = By.xpath("(//input[@name='shareholder_ownership[]'])[2]");
    public By shareholderPassport2 = By.xpath("(//input[@name='shareholder_passport_no[]'])[2]");
    public By shareholderCitizenship2 = By.xpath("(//input[@name='shareholder_citizenship[]'])[2]");
    public By shareholderContactNo2 = By.xpath("(//input[@name='shareholder_contact_no[]'])[2]");
    
    public By NRICPassportFront1 = By.xpath("(//button[@class=\"file-upload-button\"])[1]");
    public By NRICPassportFront2 = By.xpath("//*[@id='Foreign']/div[14]/div/div[1]/div/div/button");
    public By NRICPassportFront3 = By.xpath("//*[@id='Individual']/div[6]/div/div[1]/div/div/button");
    public By NRICPassportFront4 = By.xpath("//*[@id='Experienced']/div[7]/div/div[1]/div/div/button");
  
  
    public By NRICPassportBack = By.xpath("(//button[@class=\"file-upload-button\"])[2]");
    public By NRICPassportBack2 = By.xpath("//*[@id='Foreign']/div[14]/div/div[2]/div/div/button");
    public By NRICPassportBack3 = By.xpath("//*[@id='Individual']/div[6]/div/div[2]/div/div/button");
    public By NRICPassportBack4 = By.xpath("//*[@id='Experienced']/div[7]/div/div[2]/div/div/button");
  
    public By certificateofIncorporation = By.xpath("//*[@id='Foreign']/div[14]/div/div[3]/div/div/button");
  
    public By proofofResidence = By.xpath("(//button[@class=\"file-upload-button\"])[3]");
    public By proofofResidence2	 = By.xpath("//*[@id='Foreign']/div[14]/div/div[5]/div/div/button");
    public By proofofResidence3	 = By.xpath("//*[@id='Individual']/div[6]/div/div[3]/div/div/button");
    public By proofofResidence4	 = By.xpath("//*[@id='Experienced']/div[7]/div/div[3]/div/div/button");
    public By STWCMand = By.xpath("//*[@id='WSBorrowerUploaddoc']/div/div/div/div[1]/div[4]/div/div/button");
 
 
    public By memoandArticles = By.xpath("(//button[@class=\"file-upload-button\"])[4]");
    public By memoandArticles2 = By.xpath("//*[@id='Foreign']/div[14]/div/div[4]/div/div/button");
 
    public By acraBizfile = By.xpath("(//button[@class='file-upload-button'])[5]");
    public By actupload6 = By.xpath("(//button[@class='file-upload-button'])[6]");
    public By actupload7 = By.xpath("(//button[@class='file-upload-button'])[7]");
    
    public By optionalFile = By.xpath("//*[@id='Singapore']/div[3]/div/div[6]/div/div/button");
    public By optionalFile2 = By.xpath("//*[@id='Foreign']/div[14]/div/div[6]/div/div/button");
    public By optionalFile3 = By.xpath("//*[@id='Individual']/div[6]/div/div[4]/div/div/button");
    public By optionalFile4 = By.xpath("//*[@id='Experienced']/div[7]/div/div[4]/div/div/button");
  
  
    public By agree = By.xpath("//label[@for='agree14']");
    public By agree5 = By.xpath("//label[@for='Esignagree1']");
  
    public By agree2 = By.xpath("//label[@for='agree20']");
    public By agree3 = By.xpath("//label[@for='agree50']");
    public By agree4 = By.xpath("//label[@for='agree60']");
    public By acknowledge1 = By.xpath("//label[@for='agree10']");
    public By acknowledge = By.xpath("//label[@for='agree11']");
    public By acknowledge2 = By.xpath("//label[@for='agree21']");
    public By acknowledge3 = By.xpath("//label[@for='agree51']");
    public By acknowledge4 = By.xpath("//label[@for='agree61']");
    public By termsandCondition = By.xpath("(//*[@id='checkbox']/label/span[2])[4]");
    public By termsandCondition2 = By.xpath("//label[@for='agree22']");
    public By termsandCondition3 = By.xpath("//label[@for='agree52']");
    public By termsandCondition4 = By.xpath("//label[@for='agree62']");
    public By registerUs = By.xpath("//label[@for='agree13']");
    public By registerUs2 = By.xpath("//label[@for='agree23']");
    public By registerUs22 = By.xpath("//label[@for='agree24']");
    public By registerUs3 = By.xpath("//label[@for='agree53']");
    public By registerUs33 = By.xpath("//label[@for='agree54']");
    public By registerUs4 = By.xpath("//label[@for='agree63']");
    public By registerUs44 = By.xpath("//label[@for='agree64']");
  
  
    public By save = By.xpath("//button[@id='save']");
    public By submit = By.xpath("//*[@id='app']/div/div[2]/div/div[6]/button[2]");
    public By applyForLoan = By.xpath("//*[@id='app']/div/div[2]/div/div/button");
    public By confirmMSG = By.xpath("//button[@data-dismiss='modal']");
    public By submit5 = By.xpath("//*[@id='app']/div/div[2]/div/div[7]/button[2]");
    
    public By endSubmit = By.xpath("//*[@id='app']/div/div[2]/div/div/div[2]/div[3]/div[2]/button[2]");
    public By endSubmit1 = By.xpath("//*[@id='app']/div/div[2]/div/div/div[2]/div[3]/div[2]/button");
    public By submit2 = By.xpath("//button[@id='submit2']");
    public By submit3 = By.xpath("//button[@id='submit3']");
    public By submit4 = By.xpath("//button[@id='submit4']");
    public By smeMessage = By.xpath("//h2[@class='heading-tQ']");
    
    
    public By applicationreviewmessage = By.xpath("//h3[@class='profile-ban-txt']");
	public By successmsg = By.xpath("/html/body/div[9]/div[2]/div[1]/h3");
	
	/************************Indonesian Legal Entity************************/
	
	public By companyNameIndonesian = By.xpath("(//input[@id='CompanyName'])[3]");
	public By salutationIndonesian = By.xpath("(//select[@id='Salutation'])[3]");
	public By companyRegistrationNumberIndonesian = By.xpath("(//input[@id='CompanyRegistrationNumber'])[2]");
	public By firstNameIndonesian = By.xpath("(//input[@id='FirstName'])[3]");
	public By deedofEstablishmentIndonesian = By.xpath("(//input[@name='DeedOfEstablishment'])[3]");
	public By middleNameIndonesian = By.xpath("(//input[@name='MiddleName'])[3]");
	public By companyAddressIndonesian = By.xpath("(//input[@name='CompanyAddress'])[3]");
	public By familyNameIndonesian = By.xpath("(//input[@name='FamilyName'])[3]");
	public By nricIndonesian = By.xpath("(//input[@name='NRICNumber'])[3]");
	public By companyZipCodeIndonesian = By.xpath("(//input[@name='CompanyZipCode'])[3]");
	public By nationalityIndonesian = By.xpath("(//select[@name='Nationality'])[3]");
	public By siccCodeIndonesian = By.xpath("(//select[@name='SICCCode'])[2]");
	public By primaryContactNumberIndonesian = By.xpath("(//input[@name='PrimaryContactNumber'])[3]");
	public By designationIndonesian = By.xpath("(//input[@name='Designation'])[3]");
	public By networthPreviousyearIndonesian = By.xpath("(//input[@name='net_worth_previous_year'])[3]");
	public By networthCurrentyearIndonesian = By.xpath("(//input[@name='net_worth_previous_year'])[3]");
	public By managementNameIndonesian = By.xpath("(//input[@name='managementName[]'])[3]");
	public By directorPositionIndonesian = By.xpath("(//input[@name='director_position[]'])[3]");
	public By directorServingSinceIndonesian = By.xpath("(//input[@name='director_servingSince[]'])[3]");
	public By directorPassportnoIndonesian = By.xpath("(//input[@name='director_passport_no[]'])[3]");
	public By directorCitizenshipIndonesian = By.xpath("(//input[@name='director_citizenship[]'])[3]");
	public By directorContactNoIndonesian = By.xpath("(//input[@name='director_contact_no[]'])[3]");
	public By shareholderNameIndonesian = By.xpath("(//input[@name='shareholderName[]'])[3]");
	public By shareholderOwnershipIndonesian = By.xpath("(//input[@name='shareholder_ownership[]'])[3]");
	public By shareholderPassportNoIndonesian = By.xpath("(//input[@name='shareholder_passport_no[]'])[3]");
	public By shareholderCitizenshipIndonesian = By.xpath("(//input[@name='shareholder_citizenship[]'])[3]");
	public By shareholderContactnoIndonesian = By.xpath("(//input[@name='shareholder_contact_no[]'])[3]");
	public By file1Indonesian = By.xpath("//*[@id='IndoEntityInvesterForm']/div[14]/div/div[1]/div/div/button");
	public By file2Indonesian = By.xpath("//*[@id='IndoEntityInvesterForm']/div[14]/div/div[2]/div/div/button");
	public By file3Indonesian = By.xpath("//*[@id='IndoEntityInvesterForm']/div[14]/div/div[3]/div/div/button");
	public By file4Indonesian = By.xpath("//*[@id='IndoEntityInvesterForm']/div[14]/div/div[4]/div/div/button");
	public By file5Indonesian = By.xpath("//*[@id='IndoEntityInvesterForm']/div[14]/div/div[5]/div/div/button");
	public By check1Indonesian = By.xpath("//label[@for='agree30']");
	public By check2Indonesian = By.xpath("//label[@for='agree31']");
	public By check3Indonesian = By.xpath("//label[@for='agree32']");
	public By check4Indonesian = By.xpath("//label[@for='agree33']");
	public By check5Indonesian = By.xpath("//label[@for='agree34']");
	public By submitIndonesian = By.xpath("//button[@id='IndoEntityInvesterFormSubmitBtn']");
	
	/*************************** Foreign Legal Entity *************************************/
	
	public By foreignLegalEntity = By.xpath("//input[@id='inlineRadio6']");
	public By comapanyNameForeign = By.xpath("(//input[@id='CompanyName'])[4]");
	public By salutationForeign = By.xpath("(//select[@id='Salutation'])[4]");
	public By companyRegistrationForeign = By.xpath("(//input[@id='CompanyRegistrationNumber'])[3]");
	public By firstNameForeign = By.xpath("(//input[@id='FirstName'])[4]");
	public By deedofEstbForeign = By.xpath("(//input[@name='DeedOfEstablishment'])[4]");
	public By middleNameForeign = By.xpath("(//input[@name='MiddleName'])[4]");
	public By companyAddressForeign = By.xpath("(//input[@name='CompanyAddress'])[4]");
	public By familyNameForeign = By.xpath("(//input[@name='FamilyName'])[4]");
	public By nricForeign = By.xpath("(//input[@name='NRICNumber'])[4]");
	public By companyZipCodeForeign = By.xpath("(//input[@name='CompanyZipCode'])[4]");
	public By nationalityForeign = By.xpath("(//select[@name='Nationality'])[4]");
	public By siccForeign = By.xpath("(//select[@name='SICCCode'])[3]");
	public By primaryContactNumberForeign = By.xpath("(//input[@name='PrimaryContactNumber'])[4]");
	public By designationForeign = By.xpath("(//input[@name='Designation'])[4]");
	public By netWorthPreviousYearForeign = By.xpath("(//input[@name='net_worth_previous_year'])[4]");
	public By netWorthCurrentYearForeign = By.xpath("(//input[@name='net_worth_current_year'])[4]");
	public By managemnetNameForeign = By.xpath("(//input[@name='managementName[]'])[4]");
	public By directorPositionForeign = By.xpath("(//input[@name='director_position[]'])[4]");
	public By directorServingSinceForeign = By.xpath("(//input[@name='director_servingSince[]'])[4]");
	public By directorPassportnoForeign = By.xpath("(//input[@name='director_passport_no[]'])[4]");
	public By directorCitizenshipForeign = By.xpath("(//input[@name='director_citizenship[]'])[4]");
	public By directorContactNoForeign = By.xpath("(//input[@name='director_contact_no[]'])[4]");
	public By shareholderNameForeign = By.xpath("(//input[@name='shareholderName[]'])[4]");
	public By shareholderOwnerShipForeign = By.xpath("(//input[@name='shareholder_ownership[]'])[4]");
	public By shareholderPassportForeign = By.xpath("(//input[@name='shareholder_passport_no[]'])[4]");
	public By shareholderCitizenForeign = By.xpath("(//input[@name='shareholder_citizenship[]'])[4]");
	public By shareholderContactForeign = By.xpath("(//input[@name='shareholder_contact_no[]'])[4]");
	public By file1Foreign = By.xpath("//*[@id='ForeignEntityInvestorForm']/div[14]/div/div[1]/div/div/button");
	public By file2Foreign = By.xpath("//*[@id='ForeignEntityInvestorForm']/div[14]/div/div[2]/div/div/button");
	public By file3Foreign = By.xpath("//*[@id='ForeignEntityInvestorForm']/div[14]/div/div[3]/div/div/button");
	public By file4Foreign = By.xpath("//*[@id='ForeignEntityInvestorForm']/div[14]/div/div[4]/div/div/button");
	public By file5Foreign = By.xpath("//*[@id='ForeignEntityInvestorForm']/div[14]/div/div[5]/div/div/button");
	public By check1Foreign = By.xpath("//label[@for='agree40']");
	public By check2Foreign = By.xpath("//label[@for='agree41']");
	public By check3Foreign = By.xpath("//label[@for='agree42']");
	public By check4Foreign = By.xpath("//label[@for='agree43']");
	public By check5Foreign = By.xpath("//label[@for='agree44']");
	public By submitForeign = By.xpath("//button[@id='ForeignEntityInvestorFormSubmitBtn']");
	
	/******************* International Institutions *******************************/
	
	public By internationalInstitutions = By.xpath("//input[@id='inlineRadio7']");
	public By comapanyNameInternational = By.xpath("(//input[@id='CompanyName'])[5]");
	public By salutationInternational = By.xpath("(//select[@id='Salutation'])[7]");
	public By companyRegistrationInternational = By.xpath("(//input[@id='CompanyRegistrationNumber'])[4]");
	public By firstNameInternational = By.xpath("(//input[@id='FirstName'])[7]");
	public By deedofEstbInternational = By.xpath("(//input[@name='DeedOfEstablishment'])[5]");
	public By middleNameInternational = By.xpath("(//input[@name='MiddleName'])[7]");
	public By companyAddressInternational = By.xpath("(//input[@name='CompanyAddress'])[5]");
	public By familyNameInternational = By.xpath("(//input[@name='FamilyName'])[7]");
	public By nricInternational = By.xpath("(//input[@name='NRICNumber'])[7]");
	public By companyZipCodeFInternational = By.xpath("(//input[@name='CompanyZipCode'])[4]");
	public By nationalityInternational = By.xpath("(//input[@name='CompanyZipCode'])[5]");
	public By siccInternational = By.xpath("(//select[@name='SICCCode'])[4]");
	public By primaryContactNumberInternational = By.xpath("(//input[@name='PrimaryContactNumber'])[7]");
	public By designationInternational = By.xpath("(//input[@name='Designation'])[5]");
	public By netWorthPreviousYearInternational = By.xpath("(//input[@name='net_worth_previous_year'])[7]");
	public By netWorthCurrentYearFInternational = By.xpath("(//input[@name='net_worth_current_year'])[7]");
	public By managemnetNameInternational = By.xpath("(//input[@name='managementName[]'])[5]");
	public By directorPositionInternational = By.xpath("(//input[@name='director_position[]'])[5]");
	public By directorServingSinceInternational = By.xpath("(//input[@name='director_servingSince[]'])[5]");
	public By directorPassportnoInternational = By.xpath("(//input[@name='director_passport_no[]'])[5]");
	public By directorCitizenshipInternational = By.xpath("(//input[@name='director_citizenship[]'])[5]");
	public By directorContactNoInternational = By.xpath("(//input[@name='director_contact_no[]'])[5]");
	public By shareholderNameInternational = By.xpath("(//input[@name='shareholderName[]'])[5]");
	public By shareholderOwnerShipFInternational = By.xpath("(//input[@name='shareholder_ownership[]'])[5]");
	public By shareholderPassportInternational = By.xpath("(//input[@name='shareholder_passport_no[]'])[5]");
	public By shareholderCitizenInternational = By.xpath("(//input[@name='shareholder_citizenship[]'])[5]");
	public By shareholderContactInternational = By.xpath("(//input[@name='shareholder_contact_no[]'])[5]");
	public By file1International = By.xpath("//*[@id='InternationalInstitutionInvestorForm']/div[14]/div/div[1]/div/div/button");
	public By file2International = By.xpath("//*[@id='InternationalInstitutionInvestorForm']/div[14]/div/div[2]/div/div/button");
	public By file3International = By.xpath("//*[@id='InternationalInstitutionInvestorForm']/div[14]/div/div[3]/div/div/button");
	public By file4International = By.xpath("//*[@id='InternationalInstitutionInvestorForm']/div[14]/div/div[4]/div/div/button");
	public By file5International = By.xpath("//*[@id='InternationalInstitutionInvestorForm']/div[14]/div/div[5]/div/div/button");
	public By check1International = By.xpath("//label[@for='agree70']");
	public By check2International = By.xpath("//label[@for='agree71']");
	public By check3International = By.xpath("//label[@for='agree72']");
	public By check4International = By.xpath("//label[@for='agree73']");
	public By check5International = By.xpath("//label[@for='agree74']");
	public By submitInternational = By.xpath("//button[@id='InternationalInstitutionInvestorFormSubmitBtn']");
	
	
	
	/****************************SME**********************/
	
	public By entityType = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[3]/div[2]/select");
	public By companySector = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[4]/div[2]/select");
	public By SMECompanyAddress = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[5]/div[2]/input");
	public By district = By.xpath("//input[@id='districts']");
	public By countyTown = By.xpath("//input[@id='countyTown']");
	public By provinvce =  By.xpath("//input[@id='Province']");
	public By SMECompanyPostalCode = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[6]/div[2]/input");
	public By SMEDesignation = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[5]/div[2]/input");
	public By aboutValidus = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[6]/div[2]/select");
	public By uploadFile1 = By.xpath("(//*[@id='__fs_file_input'])[1]");
	public By uploadFile2 = By.xpath("(//input[@id='__fs_file_input'])[2]");
	public By uploadFile3 = By.xpath("(//input[@id='__fs_file_input'])[3]");
	public By uploadFile4 = By.xpath("(//input[@id='__fs_file_input'])[4]");
	public By uploadFile5 = By.xpath("(//input[@id='__fs_file_input'])[5]");
	public By uploadFile6 = By.xpath("(//input[@id='__fs_file_input'])[6]");
	public By uploadFile7 = By.xpath("(//input[@id='__fs_file_input'])[7]");
	public By uploadFile8 = By.xpath("(//input[@id='__fs_file_input'])[8]");
	public By uploadFile9 = By.xpath("(//input[@id='__fs_file_input'])[9]");
	
	public By agreeSME = By.xpath("//label[@for='agree']");
	public By acknowledgeSME = By.xpath("//label[@for='agree2']");
	public By termsandConditionSME = By.xpath("(//*[@id='checkbox']/label/span[2])[4]");
	public By termsandConditionSME1 = By.xpath("//label[@for='agree4']");
	
	
	public By NRICfrontBack = By.xpath("//*[@id='WSBorrowerApplicationDetails']/div/div[1]/div/div/button");
	public By passportSME = By.xpath("//*[@id='WSBorrowerApplicationDetails']/div/div[2]/div/div/button");
	public By ACRAbizfileSME = By.xpath("//*[@id='WSBorrowerApplicationDetails']/div/div[2]/div/div/button");
	public By MemorandumfileSME = By.xpath("//*[@id='WSBorrowerApplicationDetails']/div/div[4]/div/div/button");
	public By addressProofSME = By.xpath("//*[@id='WSBorrowerApplicationDetails']/div/div[5]/div/div/button");
	public By optionalfileSME = By.xpath("//*[@id='WSBorrowerApplicationDetails']/div/div[6]/div/div/button");
	public By accountOwner = By.xpath("//input[@name='NameAsPerBank']");
	public By bankName = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[3]/div[2]/select");
	public By accountNumber = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[2]/div[2]/input");
	public By branchCode = By.xpath("//input[@id='BranchCode']");
	public By next = By.xpath("//button[@id='submit']");
	public By fullName = By.xpath("//*[@id='app']/div/div[2]/div/div[1]/div[2]/div[2]/div[1]/div[2]/input");
	public By residentialStatus1 = By.xpath("//*[@id='app']/div/div[2]/div/div[1]/div[2]/div[2]/div[2]/div[2]/select");
	public By NRICPassport = By.xpath("//*[@id='app']/div/div[2]/div/div[1]/div[2]/div[2]/div[3]/div[2]/input");
	public By emailID = By.xpath("//*[@id='app']/div/div[2]/div/div[1]/div[2]/div[2]/div[4]/div[2]/input");
	public By mobileNumber = By.xpath("//*[@id='app']/div/div[2]/div/div[1]/div[2]/div[2]/div[5]/div[2]/div/input");
	//*************************INVOICE FINANCING****************************//
	
	public By invoiceFinancing = By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[2]/div/div[2]/label[2]/span");
	public By workingCapitalFinancing = By.xpath("//label[@for='LoanProductType2']");
	public By invoiceFinancing_CorporateVendor  = By.xpath("//label[@for='LoanProductType3']");
	public By workingCapitalFinancing_SinglePayment  = By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[2]/div/div[2]/label[4]/span");
	public By  invoiceFinancing_CardFacility  = By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[2]/div/div[2]/label[3]/span");
	public By purchaseOrderFinancing = By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[2]/div/div[2]/label[5]/span");
	
	public By loanamount = By.xpath("(//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[1]/div[2]/input)[1]");
	public By loanamount1 = By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[2]/div[2]/input");

	public By tenureRequired = By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[2]/div[2]/select");
	public By tenureRequired1 = By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[3]/div[3]/div[2]/select");

	public By noofEmployees = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[7]/div[2]/input");
	public By annualRevenuepreviousyear = By.xpath("//input[@id='AnnualRevenuePreviousYear']");
	public By annualRevenueLatestyear = By.xpath("//input[@id='AnnualRevenueCurrentYear']");
	public By netProfitLossPreviousyear = By.xpath("//input[@id='NetProfitPreviousYear']");
	public By netProfitLossLatestyear = By.xpath("//input[@id='NetProfitCurrentYear'] ");
	public By personalGuarantor1 =By.xpath("//input[@id='NamePG1'] ");
	public By residentialStatus = By.xpath("//select[@id='ResidentialStatusPG1'] ");
	
	public By invoiceFinancingUpload1_WCSP = By.xpath("(//div[@class='custom-file-upload']//div/button)[7]");
	public By invoiceFinancingUpload2_WCSP = By.xpath("(//div[@class='custom-file-upload']//div/button)[8]");
	
	public By invoiceFinancingUpload1 = By.xpath("(//input[@id='__fs_file_input'])[1]");
	public By invoiceFinancingUpload2 = By.xpath("(//input[@id='__fs_file_input'])[2]");
	public By invoiceFinancingUpload3 = By.xpath("(//input[@id='__fs_file_input'])[3]");
	public By invoiceFinancingUpload4 = By.xpath("(//input[@id='__fs_file_input'])[4]");
	public By invoiceFinancingUpload5 = By.xpath("(//input[@id='__fs_file_input'])[5]");
	public By invoiceFinancingUpload6 = By.xpath("(//input[@id='__fs_file_input'])[6]");
	public By invoiceFinancingUpload7 = By.xpath("(//div[@class='custom-file-upload']//div/button)[15]");
	public By invoiceFinancingUpload8 = By.xpath("(//div[@class='custom-file-upload']//div/button)[16]");
	public By invoiceFinancingUpload9 = By.xpath("(//div[@class='custom-file-upload']//div/button)[17]");
	
	public By invoiceFinancingUpload10 = By.xpath("(//div[@class='custom-file-upload']//div/button)[18]");
	public By invoiceFinancingUpload11 = By.xpath("(//div[@class='custom-file-upload']//div/button)[19]");
	public By invoiceFinancingUpload12 = By.xpath("(//div[@class='custom-file-upload']//div/button)[20]");
	
	
	public By cbsFileforAllDirectors = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[3]/div/div/button");
	public By fileCbsSME = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[3]/div/div/button");
	public By cbsfileforallpersonalguarantor = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[4]/div/div/button");
	public By bankstatementforlast6months = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[5]/div/div/button");
	public By fivefileupload = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[6]/div/div/button");
	public By sixfileUpload = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[7]/div/div/button");
	public By sevenfileUpload = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[8]/div/div/button");
	public By eightFileUpload = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[9]/div/div/button");
	public By nineFileUpload = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[10]/div/div/button");
	public By tenFileUplaod = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[3]/div/div/button");
	public By elevenFileUpload = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[4]/div/div/button");
	public By incometaxassessment = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[7]/div/div/button");
	public By incometaxassessmentdirectors = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[8]/div/div/button");
	public By incometaxassessmentpersonalGuarantors = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[9]/div/div/button");
	public By accountreceivablelast12months = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[1]/div[10]/div/div/button");
	public By lastFinancialstatements = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[3]/div/div/button");
	public By yeartodatemanagement = By.xpath("//*[@id=\"savenewallproducts\"]/div[1]/div[14]/div/div/div/div[4]/div/div/button");
	public By optionalfilesSME1 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[3]/div/div/button");
	public By certifiedtrueExtracts = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[4]/div/div/button");
	public By confirmAccountprofile = By.xpath("(//*[@id='checkbox']/label/span[2])[1]");
	public By agreetobond = By.xpath("(//*[@id='checkbox']/label/span[2])[2]");
	public By disclosureinformation = By.xpath("(//*[@id='checkbox']/label/span[2])[3]");
	public By righttorejectafamily = By.xpath("(//*[@id='checkbox']/label/span[2])[4]");
	public By accuracyandAuthentication = By.xpath("(//*[@id='checkbox']/label/span[2])[5]");
	public By confirmthestatements = By.xpath("//*[@id='savenewallproducts']/div[2]/div/ul/li[6]/label/span");
	  public By agree6 = By.xpath("(//*[@id='checkbox']/label/span[2])[6]");
	  public By acceptAll = By.xpath("(//*[@id='checkbox']/label/span[2])[7]");
	public By productSubmit = By.xpath("//button[@id='submit']");
	
	
	
	//*********************Invoice Financing(Corporate Vendor Financing)**********************//
	
	public By invoicefinancingcvf = By.xpath("//label[@for='LoanProductType3']");
	public By corporate = By.xpath("//select[@id='allpartnerslist']");
	public By requestamount = By.xpath("//input[@id='expectedcreditlimit']");
	public By tenureRequired2 = By.xpath("//select[@id='cvd_loan_tenure']");
	public By last1yearfinancialStatements = By.xpath("//*[@id='savenewallproducts']/div[1]/div[2]/div[3]/div/div/div/div[1]/div/div/button");
	public By sixMonthsbankstatement = By.xpath("//*[@id='savenewallproducts']/div[1]/div[2]/div[3]/div/div/div/div[2]/div/div/button");
	public By CBSforalldirectors = By.xpath("//*[@id='savenewallproducts']/div[1]/div[2]/div[3]/div/div/div/div[3]/div/div/button");
	public By noticeofAssesments = By.xpath("//*[@id='savenewallproducts']/div[1]/div[2]/div[3]/div/div/div/div[4]/div/div/button");
	public By sixmonthsaccounts = By.xpath("//*[@id='savenewallproducts']/div[1]/div[2]/div[3]/div/div/div/div[5]/div/div/button");
	public By boardofDirectors = By.xpath("//*[@id='savenewallproducts']/div[1]/div[2]/div[3]/div/div/div/div[6]/div/div/button");
	public By annualtaxableIncomepreviousyear = By.xpath("//input[@id='TaxIncomePreviousYearPG1']");
	public By annualtaxableIncomelatestyear = By.xpath("//input[@id='TaxIncomeCurrentYearPG1']");
	
	
	//******************************Working Capital Financing**************************************//
	
	public By workingcapitalFinancing = By.xpath("//label[@for='LoanProductType2']");
	public By cbsfileforalldirectors3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[2]/div/div/button");
	public By cbsfileforallpersonalguarantor3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[3]/div/div/button");
	public By bankstatementforlast6months3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[4]/div/div/button");
	public By incometaxassessment3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[5]/div/div/button");
	public By incometaxassessmentdirectors3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[6]/div/div/button");
	public By incometaxassessmentpersonalGuarantors3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[7]/div/div/button");
	public By accountreceivablelast12months3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[8]/div/div/button");
	public By lastFinancialstatements3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[9]/div/div/button");
	public By yeartodatemanagement3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[1]/div[10]/div/div/button");
	public By optionalfilesSME3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[3]/div/div/button");
	public By certifiedtrueExtracts3 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[4]/div/div/button");
	
	
	//*********************************Working Capital Financing(SIngle Payment)*************************//
	
	public By workingcapitalFinancingsinglepayment = By.xpath("//label[@for='LoanProductType4']");
	public By workContractDocument = By.xpath("//*[@id='purchaseorderdocument']/div[1]/div/div/button");
	public By workProgress = By.xpath("//*[@id='purchaseorderdocument']/div[2]/div/div/button");
	
	
	//******************************Invoice financing(Card Facility)**************************************//
	
	public By invoiceFinancingCardFacility = By.xpath("//*[@id='app']/div/div[2]/div/div/div[1]/div/div[2]/div/div[2]/label[3]/span");
	public By invoiceDebtor = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[2]/div/div/button");
	public By optionalfilesSME4 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[3]/div/div/button");
	public By certifiedtrueExtracts4 = By.xpath("//*[@id='savenewallproducts']/div[1]/div[14]/div/div/div/div[4]/div/div/button");
	
	
	
	//******************************************Investor Status***********************************************//
	
	public By investorstatus = By.xpath("//h3[@class='profile-ban-txt']");
	public By logout = By.xpath("//a[contains(text(),'Logout')]");
	public By platform = By.xpath("//a[@id='tog-menu-a']"); 
	public By bank = By.xpath("(//i[@class='fa fa-bank'])[2]");
	public By edit = By.xpath("//button[@id='Edit']");
	public By AccountOwner = By.xpath("//select[@name='NameAsPerBank']");
	public By BankName = By.xpath("//input[@id='BankName']");
	public By AccountNumber = By.xpath("//input[@id='BankAccountNumber']");
	public By submitbank = By.xpath("//button[@id='Bankdetailssubmitbutton']");
	public By BranchCode = By.xpath("//input[@id='BranchCode']");
	
	public By accountSummary = By.xpath("//html/body/div[3]/div[2]/ul/li[5]/ul/li[1]/a");
	public By allLivefacilities = By.xpath("//a[@href='http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/investor/all_live_facilities']");
	public By availableFunds = By.xpath("//div[@class='availableFunds']"); 
	public By accountSummaryDetails = By.xpath("//div[@class=\"col-md-8 nopadding\"]"); 
	public By accountSummaryDetailsList = By.xpath("//div[@class=\"col-md-8 nopadding\"]//span[@class=\"line-panel-data\"]"); 
	public By accountSummaryArrow = By.xpath("//html/body/div[9]/div[2]/div[3]/div/div[1]/div[5]/i"); 
	
	public By danai = By.xpath("//*[@id=\"myTable\"]/tbody/tr/td/a[text()='Danai']"); 
	
	public By amtLeft = By.xpath("//*[@id=\"amtleft\"]"); 
	public By availableFunds_AllLiveFecilities = By.xpath("//div[@class=\"md-head\"]//span[@class=\"net-balance\"]");
	
	public By investmentAmount = By.xpath("//*[@id=\"investmentAmount\"]"); 
	public By fundamtclose = By.xpath("//*[@id=\"fundamtclose\"]"); 
	public By fundamtclose2 = By.xpath("//*[@id=\"submit\"]"); 
	
	
	public By loanProvisionchkbx1 = By.xpath("(//*[@id=\"condition\"]/label)[1]"); 
	public By loanProvisionchkbx2 = By.xpath("(//*[@id='condition']/label)[2]"); 
	public By loanProvisionchkbx3 = By.xpath("(//*[@id='condition']/label)[3]"); 
	public By loanProvisionchkbx4 = By.xpath("(//*[@id='condition']/label)[4]"); 
	public By confirm = By.xpath("//*[@id='submit']"); 
	public By ok = By.xpath("//*[@id=\"cnf\"]");
	public By withDraw = By.xpath("//div[@class='menu']//li/a[@id=\"with-draw\"]");
	public By pullAmount = By.xpath("//*[@id='amount']");
	public By withDrawSubmit = By.xpath("//*[@id=\"wdraw\"]");
	public By withDrawalBalance = By.xpath("//div[@class=\"remodal remodal-is-initialized remodal-is-opened\"]//span[@class=\"net-balance\"]");
	
	public By withDrawConfirm = By.xpath("//*[@id='wdraw1']");
	public By withDrawClosed = By.xpath("//*[@id='wdraw2']");
	public By batumbulogout = By.xpath("//a[@href='http://batumbu.com/pinjaman_bahasa_enkripsi/platform/index.php/home/logout']");
	
	//******************************************ESignature***********************************************//
	
		public By esignatureClickhere = By.xpath("//div[@class='content-layout']//div[@class='page-title']//h3[@class='profile-ban-txt']/a");
		public By firstcheckbox = By.xpath("//input[@name=\"Firstcheckbox\"]");
		public By Secondcheckbox = By.xpath("//input[@name='secondcheckbox']");
		public By esignatureAgree = By.xpath("//div[@class='modal-footer']//button[@class='btn btn-danger btn-flat TermsAcceptModalButton']");
		
		public By captcha = By.xpath("//div[@class='recaptcha-checkbox-checkmark']");
		public By esignatureSubmit = By.xpath("//button[@id='submitbutton']");
		
		public By smeConfirm=	By.xpath("//button[@id='Confirmationmodalbtn']");
		
		public By next1 = By.xpath("//*[@id='app']/div/div[2]/div/div[3]/button[2]");
		public By next2 = By.xpath("//*[@id='app']/div/div[2]/div/div[5]/button[2]");
		public By next3 = By.xpath("//*[@id='app']/div/div[2]/div/div[3]/button");
		public By next4 = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/button");
		
		public By cvf = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/div[2]/label[1]/span");
		public By buyersCompanyName = By.xpath("//*[@id='app']/div/div[2]/div/div[3]/div/div/div[2]/select");

}












