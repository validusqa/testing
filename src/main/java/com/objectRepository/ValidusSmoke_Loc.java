package com.objectRepository;

import org.openqa.selenium.By;

public class ValidusSmoke_Loc {

	
	public By validusMainpageLogin = By.xpath("//a[contains(text(),'Login')]");
	public By validusCreateanAccount = By.xpath("//a[@class='create-acc']");
	public By validusInvestor = By.xpath("//button[contains(text(),'Penyedia Dana')]");
	public By validusSME = By.xpath("//div[@class='buttons-register']//a/button[text()='Wira UKM']");
	public By Setuju1 = By.xpath("//a[text()='Setuju' and @data-dismiss='modal']");
	public By Setuju2_Investor = By.xpath("//a[text()='Setuju' and @id='requiredDocumentsubmitBtn']");
	public By Setuju2_Investor_Textbx = By.xpath("//input[@id='commitedFundId']");
	public By singaporeSME = By.xpath("//button[contains(text(),'SME')]");
	public By emailIDInvestor = By.xpath("//*[@id='app']/div/div[2]/div[2]/div/div[2]/div/div[1]/div[2]/input");
    public By confirmPasswordInvestor = By.xpath("(//input[@type='password'])[2]");
    public By passwordInvestor = By.xpath("(//input[@type='password'])[1]");
    public By mobileNumber = By.xpath("//*[@id='app']/div/div[2]/div[2]/div/div[2]/div/div/form/div[3]/div[2]/div/input");
    
    public By investorLoginTermsandCondition = By.xpath("(//span[@id='ibc'])[1]");
    public By SmeloginTermsandCondition = By.xpath("(//span[@class='checkmark'])[3]");
    public By investorContinue = By.xpath("//*[@id='app']/div/div[2]/div[2]/div/div[2]/div/div/div[2]/button");
    
    public By securityCheckbox = By.xpath("//input[@name='secondcheckbox']");
    public By securityContinue = By.xpath("//button[text()='Setuju']");
    
    public By otpContent = By.xpath("//label[@class='active']");
    public By otpscreenInvestor = By.xpath("//input[@id='otpEntered']");
    public By otpContinue = By.xpath("//a[@class='continue-register']");
    public By otpResend = By.id("resendOTP");
    public By otpConfirmationMessage = By.xpath("//h2[@class='heading-tQ']");
    public By emailConfirmationMessage = By.xpath("//a[contains(text(),'login into validus')]");
    public By logOut = By.xpath("//a[contains(text(),'Logout')]");
    public By details = By.xpath("//h3[@class='profile-ban-txt']");
    public By bank = By.xpath("//a[contains(text(),'Bank')]");
    public By accountHolderName= By.xpath("//input[@id='NameAsPerBank']");
    public By bankName = By.xpath("//input[@id='BankName']");
    public By accountName = By.xpath("//input[@id='BankAccountNumber']");
    public By branchCode = By.xpath("//input[@id='BranchCode']");	
    public By edit = By.xpath("//button[@id='Edit']");
    public By save = By.xpath("//button[@id='Save']");
    public By platform = By.xpath("//a[contains(text(),'PLATFORM')]");
    public By allLivefacilities = By.xpath("//a[contains(text(),' All Live Facilities')]");
    public By availableFunds = By.xpath("//div[@class='availableFunds']");
    
    public By otpMessage = By.xpath("//input[@id='ip_otp']");
    public By otpSubmit = By.xpath("//button[@type='submit']");
    public By continueBtn = By.xpath("//a[@value=\"Submit\"]");
    		
	
}
